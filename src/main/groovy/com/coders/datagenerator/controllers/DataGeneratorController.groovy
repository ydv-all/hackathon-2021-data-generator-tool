package com.coders.datagenerator.controllers

import com.coders.datagenerator.models.Body


@RestController
@RequestMapping('generate')
class DataGeneratorController {

    @PostMapping("")
    String generate(@RequestBody Body body) {
      return body
    }

}
