package com.coders.datagenerator.models

// NumberItems,
// Flag for Customer/Guest,
// Flag for Payment Method (Stripe/Authorize.net),
// delivery method (pickup/shipping),
// Checkout Version 2.0/4.0

class Body {
  String numberItems
  String customerType
  String paymentProcessor
  String deliveryMethod
  String checkoutType

}
