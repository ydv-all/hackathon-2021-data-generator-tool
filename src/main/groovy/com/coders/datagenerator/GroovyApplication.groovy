package com.coders.datagenerator

import org.springframework.boot.SpringApplication

@SpringBootApplication
class GroovyApplication {
    static void main(String[] args) {
        SpringApplication.run GroovyApplication, args
    }
}
