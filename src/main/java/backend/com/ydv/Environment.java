package backend.com.ydv;


import org.aeonbits.owner.Config;


@Config.Sources({
        "classpath:./Properties/${environment}.properties"
})

public interface Environment extends Config {
    @Key("PAY_X_API_KEY")
    public String PAY_X_API_KEY();

    @Key("BASE_ENDPOINT_COMMERCE_BL")
    public String BASE_ENDPOINT_COMMERCE_BL();

    @Key("BASE_ENDPOINT_COPILOT_BL")
    public String BASE_ENDPOINT_COPILOT_BL();

    @Key("MONGO_ACCOUNT_ID")
    public String MONGO_ACCOUNT_ID();

    @Key("STAGE")
    public String STAGE();

    @Key("ACCOUNT_ID")
    public String ACCOUNT_ID();

    @Key("EMAIL_ID")
    public String EMAIL_ID();

    @Key("PASSWORD")
    public String PASSWORD();

    @Key("COMMERCE_USER")
    public String COMMERCE_USER();

    @Key("COMMERCE_PASSWORD")
    public String COMMERCE_PASSWORD();

    @Key("MONGO_STRING")
    public String MONGO_STRING();
    @Key("DATABASE_PIM")
    public String DATABASE_PIM();
    @Key("DATABASE_OFFER")
    public String DATABASE_OFFER();
    @Key("DATABASE_INVENTORY")
    public String DATABASE_INVENTORY();

    @Key("ITEM_ID1")
    public String ITEM_ID1();
    @Key("ITEM_ID2")
    public String ITEM_ID2();
    @Key("ITEM_ID3")
    public String ITEM_ID3();
    @Key("DEFAULT_PRICE_LIST_ID")
    public String DEFAULT_PRICE_LIST_ID();
    @Key("COUPON")
    public String COUPON();
}