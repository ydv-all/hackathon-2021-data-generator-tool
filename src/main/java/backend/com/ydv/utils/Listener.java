package backend.com.ydv.utils;

import com.aventstack.extentreports.Status;
import backend.com.ydv.utils.extentreports.ExtentManager;
import org.apache.log4j.Logger;
import org.testng.*;

import static backend.com.ydv.utils.extentreports.ExtentTestManager.startTest;
import static backend.com.ydv.utils.extentreports.ExtentTestManager.getTest;

public class Listener implements ITestListener, ISuiteListener {
    final static Logger logger = Logger.getLogger(Listener.class);

    private BaseClass bc = BaseClass.getInstance();

    @Override
    public void onTestFailure(ITestResult result) {
        logger.error(result.getThrowable().getMessage()+"\n");


        getTest().log(Status.FAIL, ""+result.getThrowable().getMessage());
        getTest().log(Status.INFO, bc.payloadStringForReport);
        getTest().log(Status.INFO, bc.responseStringForReport);
    }

    @Override
    public void onTestStart(ITestResult result) {
        String testName = result.getName().trim();
        logger.info(tcStartFormat(testName));

        startTest(testName, "");
        //ExtentManager.startTest(testName,"");
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        logger.info("test case *PASSED*.\n");

        getTest().log(Status.PASS, "Test passed");
        getTest().log(Status.INFO, bc.payloadStringForReport);
        getTest().log(Status.INFO, bc.responseStringForReport);
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        logger.info("test case *SKIPPED*.\n");

        getTest().log(Status.SKIP, "Test skipped");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onStart(ITestContext context) {
    }

    @Override
    public void onFinish(ITestContext context) {
        // TODO Auto-generated method stub
    }


    @Override
    public void onFinish(ISuite arg0) {
        // TODO Auto-generated method stub
        //ExtentManager.getReporter().flush();
        ExtentManager.extentReports.flush();
    }

    @Override
    public void onStart(ISuite arg0) {
        // TODO Auto-generated method stub
    }

    public String tcStartFormat(String tcName) {
        return System.lineSeparator()
                +"******************************************************************************************"+System.lineSeparator()
                +"*************************   "+tcName+"   *********************************"+System.lineSeparator()
                +"******************************************************************************************";
    }
}
