package backend.com.ydv.utils;

import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import org.apache.log4j.Logger;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;

public class DBHandle {
    final static Logger logger = Logger.getLogger(DBHandle.class);

    //Class instance
    private static DBHandle instance;

    //Util variables
    private BaseClass bc;
    private String connectionString;
    private MongoClient mongoClient;
    private static MongoDatabase pimDB;
    private static MongoDatabase offersDB;
    private static MongoDatabase inventoryDB;

    /*		CONSTRUCTOR		*/
    public DBHandle(){
        bc = BaseClass.getInstance();
        connectionString = bc.testEnvironment.MONGO_STRING();
        mongoClient = MongoClients.create(connectionString);
        pimDB = mongoClient.getDatabase(bc.testEnvironment.DATABASE_PIM());
        offersDB = mongoClient.getDatabase(bc.testEnvironment.DATABASE_OFFER());
        inventoryDB = mongoClient.getDatabase(bc.testEnvironment.DATABASE_INVENTORY());
    }

    public static DBHandle getInstance() {
        if (instance == null) {
            instance = new DBHandle();
        }
        return instance;
    }

    public static List<Object> getItemsForCart(int nbrOfItems) throws ClassNotFoundException {
        List<Object> activeItems = new ArrayList<>();

        MongoCollection<Document> itemsCol = pimDB.getCollection("items");
        MongoCollection<Document> pricesCol = offersDB.getCollection("prices");
        MongoCollection<Document> inventoriesCol = inventoryDB.getCollection("inventories");


        List<Document> itemsInStock = inventoriesCol.find(and(
                Filters.size("inlocations.inTransit", 1),
                Filters.gte("inlocations.inStock",10))).into(new ArrayList<>());
        int i=0;
        for (Document itemInStock : itemsInStock) {
            System.out.println("itemsInStock: " + itemInStock.get("itemId"));
            if (pricesCol.find(eq("itemId",itemInStock.get("itemId"))).iterator().hasNext()){
                /*if(itemsCol.find(and(
                        Filters.eq("itemId",itemInStock.get("itemId")),
                        Filters.eq("isActive",true))).iterator().hasNext()) {*/
                    activeItems.add(itemInStock.get("itemId"));
                    i++;
                //}
                }
            if (i==nbrOfItems) break;
        }
        if(activeItems.size() != nbrOfItems) throw new ClassNotFoundException("Not enough items found.");
        logger.info("items For cart: " + Arrays.toString(activeItems.toArray()));
        return activeItems;
    }

    public static String defaultPriceListId() throws ClassNotFoundException {
        logger.info("Getting default priceListId.");
        MongoCollection<Document> priceListsCol = offersDB.getCollection("pricelists");

        if (priceListsCol.find(eq("name","Default")).iterator().hasNext())
            return priceListsCol.find(new  Document("name","Default")).first().get("priceListId").toString();
        throw new ClassNotFoundException("defaultPriceListId not found");
    }
}
