package backend.com.ydv.utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ValueNode;
import lombok.var;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Utils {
    final static Logger logger = Logger.getLogger(Utils.class);

    private static int DataPosition;
    private static Boolean modifyArray;
    private static CloseableHttpClient client;
    private static Random rd;

    public static CloseableHttpResponse POSTRequest(String uri, HashMap<String, String> headers, String json) throws IOException {
        try {
            client = HttpClientBuilder.create().build();
            HttpPost request = new HttpPost(uri);
            for (String header : headers.keySet())
                request.setHeader(header, headers.get(header));
            request.setEntity(new StringEntity(json, ContentType.APPLICATION_JSON));
            return client.execute(request);
        }catch(Exception e){
            logger.error("Error POSTRequest. "+ e.getMessage());
            throw e;
        }
    }
    public static CloseableHttpResponse GETRequest(String uri, HashMap<String, String> headers) throws IOException {
        try {
            client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet(uri);
            for (String header : headers.keySet())
                request.setHeader(header, headers.get(header));
            return client.execute(request);
        }catch(Exception e){
            logger.error("Error GETRequest. "+ e.getMessage());
            throw e;
        }
    }
    public static CloseableHttpResponse PATCHRequest(String uri, HashMap<String, String> headers, String json) throws IOException {
        try {
            client = HttpClientBuilder.create().build();
            HttpPatch request = new HttpPatch(uri);
            for (String header : headers.keySet())
                request.setHeader(header, headers.get(header));
            request.setEntity(new StringEntity(json, ContentType.APPLICATION_JSON));
            return client.execute(request);
        }catch(Exception e){
            logger.error("Error postRequest. "+ e.getMessage());
            throw e;
        }
    }

    public static <T> T unmarshallGeneric(String jsonBody, Class<T> clazz) throws IOException {
        logger.info("String Json to class params.");
        return new ObjectMapper()
                .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .readValue(jsonBody, clazz);
    }

    public static String javaToJson (Object clazz) throws IOException {
        logger.info("Java Object to Json string.");
        return new ObjectMapper()
                .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .writerWithDefaultPrettyPrinter().writeValueAsString(clazz);
    }

    public static String xSiteContextCommerce(String account , String stage) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
        df.setTimeZone(tz);
        String nowAsISO = df.format(new Date());
        return  "{\"date\":\""+nowAsISO+"\",\"channel\":12,\"account\":\""+account+"\",\"stage\":\""+stage+"\"}";
    }

    public static String xSiteContext() {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
        df.setTimeZone(tz);
        String nowAsISO = df.format(new Date());
        return  "{\"date\":\""+nowAsISO+"\",\"channel\":12}";
    }

    public static JsonNode generateJsonNode(String json) throws IOException {
        // Convert string to jsonNode to manipulate values.
        return new ObjectMapper()
                .readTree(json);
    }

    public static JsonNode updateJson(JsonNode node, HashMap<String, Object> testData) throws ClassNotFoundException {
        // for each item in testData map.
        for (var entry : testData.entrySet())
            node = addKeys(node, entry.getKey(),entry.getValue());
        return node;
    }

    public static JsonNode addKeysForUnknownPath(JsonNode jsonNode, String key, Object values) throws ClassNotFoundException {
        // Node that is only value. (e.g from "itemId"=123, node is 123)
        if (jsonNode.isValueNode()) {
            ValueNode valueNode = (ValueNode) jsonNode;
        }
        // Array inside Json. (e.g from "items"= [{},{}], array is [])
        else if (jsonNode.isArray()) {
            ArrayNode arrayNode = (ArrayNode) jsonNode;
            if(modifyArray){
                arrayNode.removeAll();
                for (Object i : (Object[])values)
                    arrayNode = (ArrayNode) insertValueType(arrayNode,key,i);
                modifyArray=false;
            }
            for (JsonNode node : arrayNode)
                addKeysForUnknownPath(node,key,values);
        }
        // Objects in Json. (e.g from "itemId"=123, object is "itemId")
        else if (jsonNode.isObject()) {
            ObjectNode objectNode = (ObjectNode) jsonNode;
            Iterator<Map.Entry<String, JsonNode>> iter = objectNode.fields();
            while (iter.hasNext()) {
                Map.Entry<String, JsonNode> entry = iter.next();
                if(entry.getKey().equals(key)) { // (e.g if "itemsId".equals("itemsId"))
                    Object value=values;
                    if (values instanceof List<?>)
                        value = ((ArrayList)values).get(DataPosition);
                    objectNode= (ObjectNode) insertValueType(objectNode,key,value);
                    if (value.getClass().isArray())
                        modifyArray=true;
                    DataPosition++;
                }
                addKeysForUnknownPath(entry.getValue(),key,values);
            }
        }
        return jsonNode;
    }
    public static JsonNode insertValueType(JsonNode objectNode, String key, Object value) throws ClassNotFoundException {
        // JsonNode does not insert object type so we find value type and convert it before adding to json
        if (value instanceof Integer){
            if (objectNode.isArray())
                return ((ArrayNode) objectNode).add((Integer)value);
            else
                return ((ObjectNode)objectNode).put(key, (Integer) value);}
        else if (value instanceof String){
            if (objectNode.isArray())
                return ((ArrayNode) objectNode).add((String)value);
            else
                return ((ObjectNode)objectNode).put(key,(String)value);}
        else if (value instanceof Boolean){
            if (objectNode.isArray())
                return ((ArrayNode) objectNode).add((Boolean)value);
            else
                return ((ObjectNode)objectNode).put(key, (Boolean) value);}
        else if (value instanceof Double){
            if (objectNode.isArray())
                return ((ArrayNode) objectNode).add((Double)value);
            else
                return ((ObjectNode)objectNode).put(key, (Double) value);}
        else if (value.getClass().isArray())
            return  objectNode;
        else
            throw new ClassNotFoundException("Type not found.");
    }
    public static JsonNode addKeys(JsonNode node,String key, Object value) throws ClassNotFoundException {
        String[] keys = key.split("[.]", 0);
        JsonNode subNode = node;

        //Prepare the node
        if (keys.length > 1){
            for (int i=0; i< keys.length-1;i++){
                if (isInteger(keys[i]))
                    subNode = subNode.get(Integer.parseInt(keys[i])-1);
                else
                    subNode = subNode.path(keys[i]);
            }
        }
        else {
            if (isInteger(keys[0]))
                subNode = subNode.get(Integer.parseInt(keys[0])-1);
        }

        //Insert value to node
        if (subNode.isArray()) {
            ((ArrayNode)subNode).removeAll();
            if (value.getClass().isArray())
                for (Object i : (Object[]) value)
                    insertValueType(subNode, "", i);
            else
                insertValueType(subNode, "", value);
        }
        else{
            insertValueType(subNode, keys[keys.length-1], value);
        }
        return node;
    }
    public static Boolean isInteger(String s){
        try{
            Integer.parseInt(s);
            return true;
        }catch (NumberFormatException nfe) {
            return false;
        }
    }

    public static Integer randomInt(Integer bound){
        rd = new Random();
        return rd.nextInt(bound);
    }
    public static Boolean randomBool(){
        rd = new Random();
        return rd.nextBoolean();
    }

    public static Boolean validate(String property, Object current, Object expected) throws Exception {
        String msg = "'"+property+"'. is: ["+current+"], expected: ["+expected+"]";
        if (!current.toString().equals(expected.toString())){
            //logger.info(msg);
            throw new Exception(msg);}
        else {logger.info(msg); return true;}
    }

}
