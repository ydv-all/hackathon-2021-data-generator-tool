package backend.com.ydv.utils.extentreports;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

public class ExtentManager {
    public static final ExtentReports extentReports = new ExtentReports();

    public synchronized static ExtentReports createExtentReports() {
        ExtentSparkReporter reporter = new ExtentSparkReporter("./extent-report.html");
        reporter.config().setReportName("OMS QA Test Execution Report");
        extentReports.attachReporter(reporter);
        extentReports.setSystemInfo("QA", "Mario Garay");
        return extentReports;
    }
}
