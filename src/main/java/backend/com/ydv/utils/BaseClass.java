package backend.com.ydv.utils;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import backend.com.ydv.Environment;
import org.aeonbits.owner.ConfigFactory;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BaseClass {
    final static Logger logger = Logger.getLogger(BaseClass.class);

    private static BaseClass instance;

    public CloseableHttpClient client;
    public CloseableHttpResponse response;
    public String responseJson;

    public Environment testEnvironment;
    public String domain;
    public List<Object> items;

    //Strings used to print in report.html
    public String payloadStringForReport;
    public String responseStringForReport;

    /* CONSTRUCTOR */
    public BaseClass(){
        items = new ArrayList<>();
        domain = "";
    }

    public static BaseClass getInstance() {
        if (instance == null) {
            instance = new BaseClass();
        }
        return instance;
    }

    public void getProperties(String environment){
        logger.info("Getting properties for environment " + environment);
        testEnvironment = ConfigFactory.create(Environment.class);
        ConfigFactory.setProperty("environment", environment);
        testEnvironment = ConfigFactory.create(Environment.class);
    }

    public void closeResources() throws IOException {
        try{
            logger.info("Closing resources.");
            client.close();
            response.close();
        } catch(Exception e){
            logger.error("Failed at closing resources" + e.getMessage());
            throw e;
        }
    }

    public String obtainJsonFromResponse() throws IOException {
        try {
            responseJson = EntityUtils.toString(response.getEntity());
            responseForTestReport();
            return responseJson;
        } catch (Exception e){
            logger.error("Failed at obtaining string from response." + e.getMessage());
            throw e;
        }
    }

    public void payloadForTestReport(String uri, HashMap<String, String> headers, String json){
        payloadStringForReport = "*************************** REQUEST *****************************\n\n";
        payloadStringForReport = payloadStringForReport + "URI: " + uri + "\n";

        payloadStringForReport = payloadStringForReport + "\nHEADERS:\n";
        for (String header : headers.keySet()) {
            payloadStringForReport = payloadStringForReport + header +": "+ headers.get(header) + "\n";
        }
        payloadStringForReport = payloadStringForReport + "\nREQUEST BODY:\n" +
                new GsonBuilder().setPrettyPrinting().create().toJson(new JsonParser().parse(json));
        logger.info("Payload request.\n" + payloadStringForReport);

        payloadStringForReport = payloadStringForReport.replaceAll("\n","<br>");
        payloadStringForReport = payloadStringForReport.replaceAll("\t","&nbsp;&nbsp;&nbsp;&nbsp;");
//        payloadStringForReport = payloadStringForReport.replaceAll("\s","&nbsp;");
    }

    public void responseForTestReport(){
        responseStringForReport = "*************************** RESPONSE *****************************\n\n";
        responseStringForReport = responseStringForReport + "STATUS CODE: " + response.getStatusLine().getStatusCode() +
                " " + response.getStatusLine().getReasonPhrase() + "\n";

        responseStringForReport = responseStringForReport + "RESPONSE BODY:\n";
        responseStringForReport = responseStringForReport + new GsonBuilder().setPrettyPrinting().create().toJson(new JsonParser().parse(responseJson));

        logger.info("RESPONSE.\n" + responseStringForReport);
        //logger.info("Response body:\n" + new GsonBuilder().setPrettyPrinting().create().toJson(new JsonParser().parse(jsonBody)));
        responseStringForReport = responseStringForReport.replaceAll("\n","<br>");
        responseStringForReport = responseStringForReport.replaceAll("\t","&nbsp;&nbsp;&nbsp;&nbsp;");
//        responseStringForReport = responseStringForReport.replaceAll("\s","&nbsp;");
    }
}
