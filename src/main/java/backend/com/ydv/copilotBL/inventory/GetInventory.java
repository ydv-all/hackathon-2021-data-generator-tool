package backend.com.ydv.copilotBL.inventory;

import backend.com.ydv.utils.BaseClass;
import lombok.Getter;
import lombok.Setter;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;

@Setter @Getter
public class GetInventory {
    final static Logger logger = Logger.getLogger(backend.com.ydv.commerceBL.Inventory.class);

    // getInventory body
    private Query query;
    private Data[] data;

    // getInventory.query
    @Setter @Getter
    public static class Query{
        private Number offset;
        private Number limit;
        private Number count;
    }

    // getInventory.data
    @Setter @Getter
    public static class Data{
        private String sku;
        private Number itemId;
        private Inventory inventory;
    }

    // getInventory.data.inventory
    @Setter @Getter
    public static class Inventory{
        private Number itemId;
        private InLocations[] inlocations;
    }

    // getInventory.inventory.inlocations
    @Setter @Getter
    public static class InLocations{
        private Location location;
        private Object inStock;
    }

    // getInventory.inventory.inlocations.location
    @Setter @Getter
    public static class Location{
        private Number[] channel;
        private Boolean isActive;
    }

    //Util variables
    private BaseClass bc;
    private String getInventoryEndPoint="/api-oms/inventory?offset=0&limit=10";

    /* CONSTRUCTOR */
    public GetInventory(){
        bc = BaseClass.getInstance();
        bc.client = HttpClientBuilder.create().build();
    }

    /*API request*/
    public void getInventoryAPI(String token) throws IOException {
        try{
            logger.info("getInventory API request.");
            String uri = bc.testEnvironment.BASE_ENDPOINT_COPILOT_BL() + getInventoryEndPoint;
            HashMap<String, String> headers = new HashMap<>();
            headers.put("Authorization", token);

            HttpGet request = new HttpGet(uri);
            for (String header : headers.keySet()) {
                request.setHeader(header, headers.get(header));}
            bc.payloadForTestReport(uri,headers,"");
            bc.response = bc.client.execute(request);
        } catch(Exception e){
            logger.error("Error getting inventory. "+ e.getMessage());
            throw e;
        }
    }
}


