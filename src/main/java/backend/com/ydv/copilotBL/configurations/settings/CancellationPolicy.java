package backend.com.ydv.copilotBL.configurations.settings;

import backend.com.ydv.utils.BaseClass;
import lombok.Getter;
import lombok.Setter;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;

@Getter @Setter
public class CancellationPolicy {
    final static Logger logger = Logger.getLogger(CancellationPolicy.class);

    // Response body
    private Object autoRefund;
    private Object isEnabled;
    private Object windowHours;

    //Negative response
    private Object code;
    private Object message;

    //Util variables
    private BaseClass bc;
    private String CancelEndPoint="/api-oms/setting/cancellation-policy";

    /* CONSTRUCTOR */
    public CancellationPolicy(){
        bc = BaseClass.getInstance();
        bc.client = HttpClientBuilder.create().build();
    }

    /*API request*/
    public void createCancellationPolicy(String token) throws IOException {
        try{
            logger.info("createCancellationPolicy API request.");
            String uri = bc.testEnvironment.BASE_ENDPOINT_COPILOT_BL() + CancelEndPoint;
            HashMap<String, String> headers = new HashMap<>();
            headers.put("Authorization", token);

            HttpPost request = new HttpPost(uri);
            for (String header : headers.keySet()) {
                request.setHeader(header, headers.get(header));}
            String json= generateCancellationPolicy_Json();
            bc.payloadForTestReport(uri,headers,json);
            request.setEntity(new StringEntity(json, ContentType.APPLICATION_JSON));
            bc.response = bc.client.execute(request);
        } catch(Exception e){
            logger.error("Error creating cancellation policy. "+ e.getMessage());
            throw e;
        }
    }

    /*API request*/
    public void updateCancellationPolicy(String token) throws IOException {
        try{
            logger.info("updateCancellationPolicy API request.");
            String uri = bc.testEnvironment.BASE_ENDPOINT_COPILOT_BL() + CancelEndPoint;
            HashMap<String, String> headers = new HashMap<>();
            headers.put("Authorization", token);

            HttpPatch request = new HttpPatch(uri);
            for (String header : headers.keySet()) {
                request.setHeader(header, headers.get(header));}
            String json= generateCancellationPolicy_Json();
            bc.payloadForTestReport(uri,headers,json);
            request.setEntity(new StringEntity(json, ContentType.APPLICATION_JSON));
            bc.response = bc.client.execute(request);
        } catch(Exception e){
            logger.error("Error updating cancellation policy. "+ e.getMessage());
            throw e;
        }
    }

    /*API request*/
    public void getCancellationPolicy(String token) throws IOException {
        try{
            logger.info("getCancellationPolicy API request.");
            String uri = bc.testEnvironment.BASE_ENDPOINT_COPILOT_BL() + CancelEndPoint;
            HashMap<String, String> headers = new HashMap<>();
            headers.put("Authorization", token);

            HttpGet request = new HttpGet(uri);
            for (String header : headers.keySet()) {
                request.setHeader(header, headers.get(header));}
            bc.payloadForTestReport(uri,headers,"");
            bc.response = bc.client.execute(request);
        } catch(Exception e){
            logger.error("Error getCancellationPolicy. "+ e.getMessage());
            throw e;
        }
    }

    public void initCancelPolicyParams(){
        autoRefund=true;
        isEnabled=true;
        windowHours=10;
    }

    /*Json generator*/
    public String generateCancellationPolicy_Json()
    {
        String json= "{\n" +
                "\t\"isEnabled\": "+isEnabled+",\n" +
                "\t\"autoRefund\":"+autoRefund+",\n" +
                "\t\"windowHours\": "+windowHours+"\n" +
                "}";

        return json;
    }
}
