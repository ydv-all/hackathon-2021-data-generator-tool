package backend.com.ydv.copilotBL.configurations.settings;

import backend.com.ydv.utils.BaseClass;
import lombok.Getter;
import lombok.Setter;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;

@Getter @Setter
public class ShippingSLA {
    final static Logger logger = Logger.getLogger(ShippingSLA.class);

    // Response body
    private Object _id;
    private Object name;
    private Object totalDays;
    private Object cutOffTime;
    private Object[] itemsIds;

    //Negative response
    private Object code;
    private Object message;

    //Util variables
    private BaseClass bc;
    private String shippingSlaEP="/api-oms/setting/shippingsla";

    /* CONSTRUCTOR */
    public ShippingSLA(){
        bc = BaseClass.getInstance();
        bc.client = HttpClientBuilder.create().build();
    }

    /*API request*/
    public void createShippingSLA(String token) throws IOException {
        try{
            logger.info("createShippingSLA API request.");
            String uri = bc.testEnvironment.BASE_ENDPOINT_COPILOT_BL() + shippingSlaEP;
            HashMap<String, String> headers = new HashMap<>();
            headers.put("Authorization", token);

            HttpPost request = new HttpPost(uri);
            for (String header : headers.keySet()) {
                request.setHeader(header, headers.get(header));}
            String json= generateCreateShippingSLA_Json();
            bc.payloadForTestReport(uri,headers,json);
            request.setEntity(new StringEntity(json, ContentType.APPLICATION_JSON));
            bc.response = bc.client.execute(request);
        } catch(Exception e){
            logger.error("Error createShippingSLA. "+ e.getMessage());
            throw e;
        }
    }

    /*API request*/
    public void updateShippingSLABy_objectId(String token) throws IOException {
        try{
            logger.info("updateShippingSLABy_objectId API request.");
            String uri = bc.testEnvironment.BASE_ENDPOINT_COPILOT_BL() + shippingSlaEP;
            HashMap<String, String> headers = new HashMap<>();
            headers.put("Authorization", token);

            HttpPatch request = new HttpPatch(uri);
            for (String header : headers.keySet()) {
                request.setHeader(header, headers.get(header));}
            String json= generateUpdateShippingSLAbyObjectID_Json();
            bc.payloadForTestReport(uri,headers,json);
            request.setEntity(new StringEntity(json, ContentType.APPLICATION_JSON));
            bc.response = bc.client.execute(request);
        } catch(Exception e){
            logger.error("Error updateShippingSLABy_objectId. "+ e.getMessage());
            throw e;
        }
    }

    /*API request*/
    public void updateShippingSLABy_Item(String token) throws IOException {
        try{
            logger.info("updateShippingSLABy_Item API request.");
            String uri = bc.testEnvironment.BASE_ENDPOINT_COPILOT_BL() + shippingSlaEP + "/itemid";
            HashMap<String, String> headers = new HashMap<>();
            headers.put("Authorization", token);

            HttpPatch request = new HttpPatch(uri);
            for (String header : headers.keySet()) {
                request.setHeader(header, headers.get(header));}
            String json= generateUpdateShippingSLAbyObjectID_Json();
            bc.payloadForTestReport(uri,headers,json);
            request.setEntity(new StringEntity(json, ContentType.APPLICATION_JSON));
            bc.response = bc.client.execute(request);
        } catch(Exception e){
            logger.error("Error updateShippingSLABy_Item. "+ e.getMessage());
            throw e;
        }
    }

    /*API request*/
    public void getShippingSLABy_id(String token, String itemIds) throws IOException {
        try{
            logger.info("getShippingSLABy_id API request.");
            String uri =
                    bc.testEnvironment.BASE_ENDPOINT_COPILOT_BL()+shippingSlaEP+"?itemsIds="+itemIds;
            HashMap<String, String> headers = new HashMap<>();
            headers.put("Authorization", token);

            HttpGet request = new HttpGet(uri);
            for (String header : headers.keySet()) {
                request.setHeader(header, headers.get(header));}
            bc.payloadForTestReport(uri,headers,"");
            bc.response = bc.client.execute(request);
        } catch(Exception e){
            logger.error("Error getShippingSLABy_id. "+ e.getMessage());
            throw e;
        }
    }

    /*API request*/
    public void getShippingSLABy_objectId(String token, String objectId) throws IOException {
        try{
            logger.info("getShippingSLABy_objectId API request.");
            String uri =
                    bc.testEnvironment.BASE_ENDPOINT_COPILOT_BL()+shippingSlaEP+"/by?objectid="+objectId;
            HashMap<String, String> headers = new HashMap<>();
            headers.put("Authorization", token);

            HttpGet request = new HttpGet(uri);
            for (String header : headers.keySet()) {
                request.setHeader(header, headers.get(header));}
            bc.payloadForTestReport(uri,headers,"");
            bc.response = bc.client.execute(request);
        } catch(Exception e){
            logger.error("Error getShippingSLABy_objectId. "+ e.getMessage());
            throw e;
        }
    }

    // Initialize vars
    public void initCancelPolicyParams(int nbrOfItems){
        name="Testing";
        totalDays=200;
        cutOffTime=200;
        itemsIds = new Object[nbrOfItems];
    }

    /*Json generator*/
    public String generateCreateShippingSLA_Json()
    {
        String itemsJson="";
        for(int i=0;i<itemsIds.length;i++){
            itemsJson = itemsJson + "\""+itemsIds[i].toString()+"\"";
            if (i < itemsIds.length-1){ itemsJson = itemsJson + ",";}
        }
        String json= "{\n" +
                "\t\"name\": "+name.toString()+",\n" +
                "\t\"totalDays\":"+totalDays.toString()+",\n" +
                "\t\"cutOffTime\": "+cutOffTime.toString()+"\n" +
                "\t\"itemsIds\": ["+ itemsJson + "]" +
                "}";
        return json;
    }

    /*Json generator*/
    public String generateUpdateShippingSLAbyObjectID_Json()
    {
        String json= "{\n" +
                "\t\"_id\": "+_id.toString()+",\n" +
                "\t\"totalDays\":"+totalDays.toString()+",\n" +
                "\t\"cutOffTime\": "+cutOffTime.toString()+"\n" +
                "}";
        return json;
    }

    /*Json generator*/
    public String generateUpdateShippingSLAbyItem_Json()
    {
        String json= "{\n" +
                "\t\"from_id\": "+_id.toString()+",\n" +
                "\t\"to_id\":"+totalDays.toString()+",\n" +
                "\t\"itemId\": "+cutOffTime.toString()+"\n" +
                "}";
        return json;
    }



}
