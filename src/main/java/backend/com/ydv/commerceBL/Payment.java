package backend.com.ydv.commerceBL;

import backend.com.ydv.utils.BaseClass;
import backend.com.ydv.utils.Utils;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;

@Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
public class Payment {
    final static Logger logger = Logger.getLogger(Payment.class);

    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Response {
        private TransactionDetails transactionDetails;
        private PaymentIdentifier paymentIdentifier;
        private PaymentDetails paymentDetails;
        private Object paymentMethod;
        private Object paymentKind;
        private Object amount;
        private Object currency;
        private Object conversion;
        private BillToAddress billToAddress;
        private Object id;

        private Object version;
        private Object isValid;
        private Object _id;
        private Object cartId;
        private Object createdAt;
        private Object updatedAt;
    }

    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class PaymentDetails {
        private TransactionDetails transactionDetails;
        private PaymentIdentifier paymentIdentifier;
        private PaymentStatus paymentStatus;
        private Object paymentMethod;
        private Object paymentKind;
        private Object amount;
        private Object currency;
        private Object conversion;
        private BillToAddress billToAddress;
    }

    // paymentDetails.transactionDetails
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class PaymentStatus {
        private Object cardHolderFullName;
        private Object paymentMethod;
        private Object last4;
        private Object amountCapturable;
        private Object status;
    }

    // paymentDetails.transactionDetails
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class TransactionDetails {
        private Object paymentType;
        private Object cardNumber;
        private Object expDate;
        private Object cvv;
        private Object cardHolderFullName;
        private Object metadata;
        private Object paymentToken;
    }

    // paymentDetails.paymentIdentifier
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class PaymentIdentifier {
        private Object cardIdentifier;
    }

    // paymentDetails.billToAddress
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class BillToAddress {
        private Name name;
        private Object email;
        private Phone phone;
        private Object street1;
        private Object street2;
        private Object city;
        private Object state;
        private Object country;
        private Object zipCode;
    }

    // paymentDetails.billToAddress.name
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Name{
        private Object first;
        private Object middle;
        private Object last;
    }

    // paymentDetails.billToAddress.phone
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Phone{
        private Object number;
        private Object kind;
    }

    //Util variables
    private BaseClass bc;

    /* CONSTRUCTOR */
    public Payment(){
        bc = BaseClass.getInstance();
    }

    /*********    Add Payment To Cart      **********/
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class AddPaymentToCart {
        private Object cartId;
        private PaymentDetails paymentDetails;
        private BillToAddress billToAddress;
    }
    public Payment.Response API_addPaymentToCart(Object clazz) throws Exception {
        try {
            logger.info("API_addPaymentToCart request.");
            String uri = bc.testEnvironment.BASE_ENDPOINT_COMMERCE_BL() + "/api-payment/payment";
            HashMap<String, String> headers = new HashMap<>();
            headers.put("x-site-context", Utils.xSiteContextCommerce(
                    bc.testEnvironment.MONGO_ACCOUNT_ID(), bc.testEnvironment.STAGE()));
            String json= Utils.javaToJson(clazz);
            bc.payloadForTestReport(uri, headers, json);
            bc.response = Utils.POSTRequest(uri,headers,json);
            return Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), Payment.Response.class);
        }catch(Exception e){
            logger.error("Error API_addPaymentToCart. "+ e.getMessage());
            throw e;
        }
    }
    public String Json_addPaymentToCart() {
            return  "{\n" +
                    "\t\"cartId\": \"123\",\n" +
                    "\t\"paymentDetails\": {\n" +
                    "\t\t\"transactionDetails\": {\n" +
                    "\t\t\t\"paymentType\": \"CARD_STRIPE\",\n" +
                    "\t\t\t\"paymentToken\": \"123\"\n" +
                    "\t\t},\n" +
                    "\t\t\"amount\": \"11.27\",\n" +
                    "\t\t\"currency\": \"USD\"\n" +
                    "\t},\n" +
                    "\t\"billToAddress\": {\n" +
                    "\t\t\"name\": {\n" +
                    "\t\t\t\"first\": \"John\",\n" +
                    "\t\t\t\"middle\": \"Bob\",\n" +
                    "\t\t\t\"last\": \"Smith\"\n" +
                    "\t\t},\n" +
                    "\t\t\"email\": \"johnsmith@gmail.com\",\n" +
                    "\t\t\"phone\": {\n" +
                    "\t\t\t\"number\": \"07780811973\",\n" +
                    "\t\t\t\"kind\": \"Mobile\"\n" +
                    "\t\t},\n" +
                    "\t\t\"street1\": \"600 CONGRESS AVE\",\n" +
                    "\t\t\"street2\": \"\",\n" +
                    "\t\t\"city\": \"Austin\",\n" +
                    "\t\t\"state\": \"TX\",\n" +
                    "\t\t\"country\": \"US\",\n" +
                    "\t\t\"zipCode\": \"10033\"\n" +
                    "\t}\n"+
                    "}\n";
    }
    public AddPaymentToCart Body_addPaymentToCart() throws IOException {
        return Utils.unmarshallGeneric(Json_addPaymentToCart(), AddPaymentToCart.class);
    }

    /*********    Create Stripe  Payment Method      **********/
    public Payment.Response API_createStripeMethod() throws Exception {
        try {
            logger.info("create Stripe Payment Method API request.");
            String uri = bc.testEnvironment.BASE_ENDPOINT_COMMERCE_BL() + "/ext-stripe/payment-method";
            HashMap<String, String> headers = new HashMap<>();
            headers.put("x-api-key", bc.testEnvironment.PAY_X_API_KEY());
            String json= Json_stripeMethod();
            bc.payloadForTestReport(uri, headers, json);
            bc.response = Utils.POSTRequest(uri,headers,json);
            return Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), Payment.Response.class);
        }catch(Exception e){
            logger.error("Error create Stripe Payment Method. "+ e.getMessage());
            throw e;
        }
    }
    public String Json_stripeMethod() {
        return "{\n" +
                "\t\"billing_details\": {\n" +
                "\t\t\"address\": {\n" +
                "\t\t\t\"line1\": \"3520 Millenio\",\n" +
                "\t\t\t\"line2\": \"Crowley Drive\",\n" +
                "\t\t\t\"city\": \"Vancouver\",\n" +
                "\t\t\t\"state\": \"BC\",\n" +
                "\t\t\t\"country\": \"CA\",\n" +
                "\t\t\t\"postal_code\": \"V5R 6G9\"\n" +
                "\t\t},\n" +
                "\t\t\"name\": \"First Last\",\n" +
                "\t\t\"email\": \"first@last.com\",\n" +
                "\t\t\"phone\": \"123-456-7890\"\n" +
                "\t},\n" +
                "\t\"metadata\": {},\n" +
                "\t\"card\": {\n" +
                "\t\t\t\"number\": \"4242424242424242\",\n" +
                "\t\t\t\"exp_month\": 2,\n" +
                "\t\t\t\"exp_year\": 2023,\n" +
                "\t\t\t\"cvc\": \"012\"\n" +
                "\t}\n" +
                "}\n";
    }

    /*********    Create Stripe Payment Intent      **********/
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class StripeIntent {
        private Object amount;
        private Object currency;
        private Object confirm;
        private Object payment_method;
    }
    public Payment.Response API_createStripeIntent(Object clazz) throws Exception {
        try {
            logger.info("create Stripe Payment Intent API request.");
            String uri = bc.testEnvironment.BASE_ENDPOINT_COMMERCE_BL() + "/ext-stripe/payment-intent";
            HashMap<String, String> headers = new HashMap<>();
            headers.put("x-api-key", bc.testEnvironment.PAY_X_API_KEY());
            String json= Utils.javaToJson(clazz);
            bc.payloadForTestReport(uri, headers, json);
            bc.response = Utils.POSTRequest(uri,headers,json);
            return Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), Payment.Response.class);
        }catch(Exception e){
            logger.error("Error create Stripe Payment Intent. "+ e.getMessage());
            throw e;
        }
    }
    public String Json_stripeIntent() {
        return "{\n" +
                "\t\"amount\": 9050,\n" +
                "\t\"currency\": \"USD\",\n" +
                "\t\"confirm\": false,\n" +
                "\t\"payment_method\": \"123\"\n" +
                "}\n";
    }
    public StripeIntent Body_createStripeIntent() throws IOException {
        return Utils.unmarshallGeneric(Json_stripeIntent(), StripeIntent.class);
    }


    /*********    Confirm Stripe Payment Intent      **********/
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class StripeConfirm {
        private Object payment_method;
    }
    public Payment.Response API_confirmStripeIntent(String intentId,Object clazz) throws Exception {
        try {
            logger.info("create Stripe Payment Confirm Intent API request.");
            String uri = bc.testEnvironment.BASE_ENDPOINT_COMMERCE_BL() +
                    "/ext-stripe/payment-intent/confirm/" + intentId;
            HashMap<String, String> headers = new HashMap<>();
            headers.put("x-api-key", bc.testEnvironment.PAY_X_API_KEY());
            String json= Utils.javaToJson(clazz);
            bc.payloadForTestReport(uri, headers, json);
            bc.response = Utils.POSTRequest(uri,headers,json);
            return Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), Payment.Response.class);
        }catch(Exception e){
            logger.error("Error create Stripe Payment Confirm Intent. "+ e.getMessage());
            throw e;
        }
    }
    public String Json_stripeConfirm() {
        return "{\n" +
                "\t\"payment_method\": \"9050\"\n" +
                "}\n";
    }
    public StripeConfirm Body_confirmStripeIntent() throws IOException {
        return Utils.unmarshallGeneric(Json_stripeConfirm(), StripeConfirm.class);
    }

    /*********    AUTHORIZED DOT NET      **********/
    public String authorizeDotNetJson(int nbrOfPayments) {
        String json="";
        for(int i=0;i<nbrOfPayments; i++){
            json =  json + "\t\t{\n" +
                    "\t\t\t\"transactionDetails\": {\n" +
                    "\t\t\t\t\"paymentType\": \"CARD\",\n" +
                    "\t\t\t\t\"cardNumber\": \"4111111111111111\",\n" +
                    "\t\t\t\t\"expDate\": \"1122\",\n" +
                    "\t\t\t\t\"cvv\": \"999\",\n" +
                    "\t\t\t\t\"cardHolderFullName\": \"tawseefahmade bhat\",\n" +
                    "\t\t\t\t\"metadata\": {}\n" +
                    "\t\t\t},\n" +
                    "\t\t\t\"paymentIdentifier\": {\n" +
                    "\t\t\t\t\"cardIdentifier\": \"1111\"\n" +
                    "\t\t\t},\n" +
                    "\t\t\t\"paymentMethod\": \"Visa\",\n" +
                    "\t\t\t\"paymentKind\": \"Fabric User\",\n" +
                    "\t\t\t\"amount\": \"11.27\",\n" +
                    "\t\t\t\"currency\": \"USD\",\n" +
                    "\t\t\t\"conversion\": 1,\n" +
                    "\t\t\t\"billToAddress\": {\n" +
                    "\t\t\t\t\"name\": {\n" +
                    "\t\t\t\t\t\"first\": \"tawseef\",\n" +
                    "\t\t\t\t\t\"last\": \"ahmade bhat\"\n" +
                    "\t\t\t\t},\n" +
                    "\t\t\t\t\"email\": \"tawseef@fabric.inc\",\n" +
                    "\t\t\t\t\"phone\": {\n" +
                    "\t\t\t\t\t\"number\": \"07780811973\",\n" +
                    "\t\t\t\t\t\"kind\": \"Mobile\"\n" +
                    "\t\t\t\t},\n" +
                    "\t\t\t\t\"street1\": \"600 CONGRESS AVE\",\n" +
                    "\t\t\t\t\"street2\": \"\",\n" +
                    "\t\t\t\t\"city\": \"AUSTIN\",\n" +
                    "\t\t\t\t\"state\": \"TX\",\n" +
                    "\t\t\t\t\"country\": \"US\",\n" +
                    "\t\t\t\t\"zipCode\": \"1003\"\n" +
                    "\t\t\t}\n";
            if (i == nbrOfPayments-1){ json = json + "\t\t}\n";
            } else { json = json + "\t\t},\n"; }
        }
        return json;
    }
}
