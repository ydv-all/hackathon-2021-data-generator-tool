package backend.com.ydv.commerceBL;

import backend.com.ydv.utils.BaseClass;
import backend.com.ydv.utils.Utils;
import lombok.Getter;
import lombok.Setter;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

import java.util.HashMap;

@Getter @Setter
public class Login {
    final static Logger logger = Logger.getLogger(Login.class);

    // Response body
    private String accessToken;

    private String message;

    //Util variables
    private BaseClass bc;
    private String identityEndPoint="/api-commerceIdentity/auth/local/login";

    /* CONSTRUCTOR */
    public Login(){
        bc = BaseClass.getInstance();
        bc.client = HttpClientBuilder.create().build();
    }

    /*API Request*/
    public void loginCommerce(String user, String pwd) throws Exception {
        try {
            logger.info("Login Commerce");
            String uri = bc.testEnvironment.BASE_ENDPOINT_COMMERCE_BL() + identityEndPoint;
            HashMap<String, String> headers = new HashMap<>();
            headers.put("x-site-context", Utils.xSiteContextCommerce(
                    bc.testEnvironment.MONGO_ACCOUNT_ID(),bc.testEnvironment.STAGE()));

            HttpPost request = new HttpPost(uri);
            for (String header : headers.keySet()) {
                request.setHeader(header, headers.get(header));}
            String json = "{\"username\" :\""+user+"\", \"password\": \""+pwd+"\"}";
            bc.payloadForTestReport(uri,headers,json);
            request.setEntity(new StringEntity(json, ContentType.APPLICATION_JSON));
            bc.response = bc.client.execute(request);
        } catch (Exception e){
            logger.error("Failed at login commerce. " + e.getMessage());
            throw e;
        }
    }

    public Login attemptToLoginCommerce() throws Exception {
        try {
            Login lc;
            int attempts=1;
            do {
                loginCommerce(bc.testEnvironment.COMMERCE_USER(), bc.testEnvironment.COMMERCE_PASSWORD());
                attempts++;
            } while(attempts <= 3 &&
                    bc.response.getStatusLine().getStatusCode() != 200);

            if(bc.response.getStatusLine().getStatusCode() != 200) throw new Exception("Unable to login to commerce after 3 attempts.");

            lc = Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), Login.class);
            return lc;
        } catch(Exception e){
            logger.error("Failed at login commerce. " + e.getMessage());
            throw e;
        }
    }


}
