package backend.com.ydv.commerceBL;

import backend.com.ydv.utils.BaseClass;
import backend.com.ydv.utils.Utils;
import com.fasterxml.jackson.annotation.JsonInclude;
import backend.com.ydv.copilotBL.inventory.GetInventory;
import lombok.Getter;
import lombok.Setter;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Cart {
    final static Logger logger = Logger.getLogger(Cart.class);

    /**************************    CART params     ***************/
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Response {
        // All existing response properties
        private Object _id;
        private Object deleted;
        private Object registeredUser;
        private Object cartId;
        private Items[] items;
        private AllPromosApplied[] allPromosApplied;
        private Attributes[] attributes;
        private Object createdAt;
        private Object updatedAt;
        private Object __v ;
        private TotalAmount totalAmount;
        private Object quantity;
        private Errors errors;
        private Object userId;
        private Object promoName;
        private Object promoId;
        private Object orderNumber;

        // Negative response
        private Object code;
        private Object message;
    }

    // cart.items
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Items {
        private Price price;
        private Object sample;
        private Object discountedQuantity;
        private Object[] group;
        private Object weightUnit;
        private Object isPickup;
        private Object createdAt;
        private Object updatedAt;
        private Object _id;
        private Object itemId;
        private Object quantity;
        private Object priceListId;
        private Object sku;
        private Object taxCode;
        private Object title;
        private Object weight;
        private Object lineItemId;
        private Object attributeTotalPrice;
        private TotalPrice totalPrice ;
        private Attributes[] attributes;
        private Object id;
        private ShipTo.Response shipTo;
        private Extra extra;
    }

    // cart.items.price
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Price {
        private Object sale;
        private Object base;
        private Discount discount;
        private Object currency;
        private Object _id;
        private Object itemId;
        private Offers[] offers;
    }

    // cart.items.extra
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Extra {
    }

    // cart.items.price.discount
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Discount {
        private Object price;
        private Object discountAmount;
        private PromosApplied[] promosApplied;
    }

    // cart.items.price.offers
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Offers {
        private Prices price;
        private Object _id;
        private Object kind;
        private Object channel;
        private Object startDate;
        private Object endDate;
        private Object offerCode;
    }

    // cart.items.price.discount.promosApplied
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class PromosApplied {
        private Object unit;
        private Object value;
        private On ON;
        private Object id;
        private Object promoId;
        private Object promoCode;
    }

    // cart.items.price.offers.prices
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Prices {
        private Object sale;
        private Object base;
        private Object currency;
        private Object cost;
        private Discount discount;
    }

    // cart.items.price.discount.promosApplied.on
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class On {
        private Object kind;
        private Value[] value;
    }

    // cart.items.price.discount.promosApplied.on.value
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Value {
        private  Object label;
        private  Object isSelected;
        private  Object key;
        private  Number[] methodIds;
    }

    // cart.items.totalPrice
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class TotalPrice {
        private Object currency;
        private Object amount;
    }

    // cart.items.attributes
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Attributes {
        //Attribute Details
    }

    //  cart.allPromosApplied
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class AllPromosApplied {
        private Object promoId;
        private Object promoCode;
    }

    // cart.totalAmount
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class TotalAmount {
        private Object currency;
        private Object amount;
    }

    // cart.errors
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Errors {
        private String[] inventory;
        private ErrorsPrice[] price;
        private Promo[] promo;
    }

    // cart.errors.price
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ErrorsPrice {
        private Object _id;
        private Object isSoftDeleted;
        private Object priceListId;
        private Object itemId;
    }

    // cart.errors.promo
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Promo {
        private Object promoChange;
    }

    //Util variables
    private final BaseClass bc;
    private GetInventory gi;
    private final String cartEndPoint="/api-cart/cart";

    /* CONSTRUCTOR */
    public Cart(){
        bc = BaseClass.getInstance();
        gi = new GetInventory();
    }

    // Get items available
    public List<Number> obtainItemsForCart(String token, int nbrOfItems) throws Exception {
        try{
            //sending request
            gi.getInventoryAPI(token);
            gi = Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), GetInventory.class);

            // validating if any item found
            if (gi.getData().length < nbrOfItems) throw new Exception("No products available.");

            List<Number> items = new ArrayList<Number>();

            // for each item object: if inStock>10 and isActive=true, take the item
            if (gi.getData() != null)
            for (int i=0; i<gi.getData().length; i++){
                if (gi.getData()[i].getInventory().getInlocations() != null)
                for (int j=0; j<gi.getData()[i].getInventory().getInlocations().length; j++){
                    if (gi.getData()[i].getInventory().getInlocations()[j].getLocation() != null)
                    if ((Integer) gi.getData()[i].getInventory().getInlocations()[j].getInStock() > 10
                    && gi.getData()[i].getInventory().getInlocations()[j].getLocation().getIsActive()){
                        items.add(gi.getData()[i].getItemId());
                        break;
                    }
                }
                if (items.size() == nbrOfItems) break;
            }

            if (items.size() < nbrOfItems) throw new Exception("Not enough products available.");

            return items;
        } catch(Exception e){
            logger.error("Error obtaining item for cart. "+ e.getMessage());
            throw e;
        }
    }

    /***   CreateCart -  ADD Item   ***/
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class CreateCart {
        private Object cartId;
        private Items[] items;
    }
    public Response API_createCart(Object bodyClass, Boolean isUserCart, String userAuthToken) throws Exception {
        try{
            logger.info("create_Cart API request.");

            String uri = bc.domain + cartEndPoint + "/item";

            HashMap<String, String> headers = new HashMap<>();
            headers.put("x-site-context", Utils.xSiteContextCommerce(
                    bc.testEnvironment.MONGO_ACCOUNT_ID(),bc.testEnvironment.STAGE()));
            if (isUserCart) headers.put("Authorization",userAuthToken);
            String json= Utils.javaToJson(bodyClass);
            bc.payloadForTestReport(uri,headers,json);
            bc.response = Utils.POSTRequest(uri,headers,json);
            return Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), Response.class);
        } catch(Exception e){
            logger.error("Error create_Cart. "+ e.getMessage());
            throw e;
        }
    }
    public String Json_createCart(int nbrOfItems) throws IOException {
        String itemsJson="";
        for (int i=0; i<nbrOfItems; i++){
            itemsJson= itemsJson +
                    "\t\t{\n" +
                    "\t\t\t\"itemId\": 1,\n" +
                    "\t\t\t\"quantity\": 1,\n" +
                    "\t\t\t\"group\": [\n" +
                    "\t\t\t\t\"123123123123123213212222\"" +
                    "\t\t\t\t],\n" +
                    "\t\t\t\"price\": {\n" +
                    "\t\t\t\t\"sale\": 0,\n" +
                    "\t\t\t\t\"base\": 100,\n" +
                    "\t\t\t\t\"discount\": {\n" +
                    "\t\t\t\t\t\"price\": 0,\n" +
                    "\t\t\t\t\t\"discountAmount\": 0,\n" +
                    "\t\t\t\t\t\"promosApplied\": []\n" +
                    "\t\t\t\t},\n" +
                    "\t\t\t\t\"currency\": \"USD\"\n" +
                    "\t\t\t},\n" +
                    "\t\t\t\"extra\": {}\n";
            if (i == nbrOfItems-1){ itemsJson = itemsJson + "\t\t}\n";
            } else { itemsJson = itemsJson + "\t\t},\n"; }
        }
        String json =  "{\n" +
                "\t\"cartId\": \"\",\n" +
                "\t\"items\": [\n" + itemsJson +

                "\t]\n" +
                "}";
        return json;
    }
    public CreateCart Body_createCart(int nbrOfItems) throws IOException {
        return Utils.unmarshallGeneric(Json_createCart(nbrOfItems), CreateCart.class);
    }

    /***   getCartBy -  cartID or userAuthToken   ***/
    public Response API_getCartBy(String getBy, Object value) throws Exception {
        try{
            // string param should be equal to "userAuthToken" or "cartId".
            logger.info("getCart API request.");
            String uri = bc.domain + cartEndPoint+ "?" + getBy + "=" + value.toString();
            HashMap<String, String> headers = new HashMap<>();
            headers.put("x-site-context", Utils.xSiteContextCommerce(
                    bc.testEnvironment.MONGO_ACCOUNT_ID(),bc.testEnvironment.STAGE()));
            bc.payloadForTestReport(uri,headers,"");
            bc.response = Utils.GETRequest(uri,headers);
            return Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), Response.class);
        } catch(Exception e){
            logger.error("Error setAndGetCartBy. "+ e.getMessage());
            throw e;
        }
    }

    /***   removeItemFromCart   ***/
    public Response API_removeItemFromCart(Object cartId, Object lineItemId) throws Exception {
        try{
            logger.info("removeItemFromCart API request.");
            String uri = bc.domain + cartEndPoint+ "/" + cartId.toString() + "/item/" + lineItemId.toString();
            HashMap<String, String> headers = new HashMap<>();
            headers.put("x-site-context", Utils.xSiteContextCommerce(
                    bc.testEnvironment.MONGO_ACCOUNT_ID(),bc.testEnvironment.STAGE()));
            bc.payloadForTestReport(uri,headers,"");
            bc.response = Utils.PATCHRequest(uri,headers,"");
            return Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), Response.class);
        } catch(Exception e){
            logger.error("Error removeItemFromCart. "+ e.getMessage());
            throw e;
        }
    }

    /***   emptyCartById   ***/
    public Response API_emptyCart(Object cartId) throws Exception {
        try{
            logger.info("emptyCart API request.");
            String uri = bc.domain + cartEndPoint+ "/" + cartId.toString() + "/empty";
            HashMap<String, String> headers = new HashMap<>();
            headers.put("x-site-context", Utils.xSiteContextCommerce(
                    bc.testEnvironment.MONGO_ACCOUNT_ID(),bc.testEnvironment.STAGE()));
            bc.payloadForTestReport(uri,headers,"");
            bc.response = Utils.PATCHRequest(uri, headers,"");
            return Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), Response.class);
        } catch(Exception e){
            logger.error("Error emptyCart. "+ e.getMessage());
            throw e;
        }
    }

    /***   updateItemsInCart   ***/
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class UpdateItemsInCart {
        private Items[] items;
    }
    public Response API_updateItemsInCart(Object cartId, Object bodyClass) throws Exception {
        try{
            logger.info("updateItemsInCart API request.");
            String uri = bc.domain + cartEndPoint+ "/" + cartId.toString() + "/items";
            HashMap<String, String> headers = new HashMap<>();
            headers.put("x-site-context", Utils.xSiteContextCommerce(
                    bc.testEnvironment.MONGO_ACCOUNT_ID(),bc.testEnvironment.STAGE()));
            String json= Utils.javaToJson(bodyClass);
            bc.payloadForTestReport(uri,headers,json);
            bc.response = Utils.PATCHRequest(uri, headers, json);
            return Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), Response.class);
        } catch(Exception e){
            logger.error("Error updateItemsInCart. "+ e.getMessage());
            throw e;
        }
    }
    private String Json_updateItemsInCart(int nbrOfItems) {
        String updateItems="";
        for (int j=0; j<nbrOfItems; j++){
            updateItems = updateItems + "\t\t{\n" +
                    "\t\t\t\"lineItemId\": 1,\n" +
                    "\t\t\t\"itemId\": 1,\n" +
                    "\t\t\t\"quantity\": 1,\n" +
                    "\t\t\t\"price\": {\n" +
                    "\t\t\t\t\"sale\": 0,\n" +
                    "\t\t\t\t\"base\": 1000,\n" +
                    "\t\t\t\t\"discount\": {\n" +
                    "\t\t\t\t\t\"price\": 0,\n" +
                    "\t\t\t\t\t\"discountAmount\": 0,\n" +
                    "\t\t\t\t\t\"promosApplied\": []\n" +
                    "\t\t\t\t},\n" +
                    "\t\t\t\t\"currency\": \"USD\"\n" +
                    "\t\t\t}\n";
            if (j == nbrOfItems-1){ updateItems = updateItems + "\t\t}\n";
            } else { updateItems = updateItems + "\t\t},\n"; }
        }
        String json =  "{\n" +
                "\t\"items\": [\n" +
                updateItems +
                "\t]" +
                "}";
        return json;
    }
    public UpdateItemsInCart Body_updateItemsInCart(int nbrOfItems) throws IOException {
        return Utils.unmarshallGeneric(Json_updateItemsInCart(nbrOfItems), UpdateItemsInCart.class);
    }

    /***   applyPromo   ***/
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ApplyPromo {
        private Object cartId;
        private Object promoName;
    }
    public Response API_applyPromo(Object bodyClass) throws Exception {
        try{
            logger.info("applyPromo API request.");
            String uri = bc.domain + cartEndPoint+ "/apply-promo";
            HashMap<String, String> headers = new HashMap<>();
            headers.put("x-site-context", Utils.xSiteContextCommerce(
                    bc.testEnvironment.MONGO_ACCOUNT_ID(),bc.testEnvironment.STAGE()));
            String json= Utils.javaToJson(bodyClass);
            bc.payloadForTestReport(uri,headers,json);
            bc.response = Utils.PATCHRequest(uri,headers,json);
            return Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), Response.class);
        } catch(Exception e){
            logger.error("Error applyPromo. "+ e.getMessage());
            throw e;
        }
    }
    private String Json_applyPromo() {
        String json =  "{\n" +
                "\t\"cartId\": \"123\",\n" +
                "\t\"promoName\": \"shipTrio\"\n" +
                "}";
        return json;
    }
    public ApplyPromo Body_applyPromo() throws IOException {
        return Utils.unmarshallGeneric(Json_applyPromo(), ApplyPromo.class);
    }

    /***   removePromo   ***/
    public Response API_removePromo(Object bodyClass) throws Exception {
        try{
            logger.info("removePromo API request.");
            String uri = bc.domain + cartEndPoint+ "/remove-promo";
            HashMap<String, String> headers = new HashMap<>();
            headers.put("x-site-context", Utils.xSiteContextCommerce(
                    bc.testEnvironment.MONGO_ACCOUNT_ID(),bc.testEnvironment.STAGE()));
            String json= Utils.javaToJson(bodyClass);
            bc.payloadForTestReport(uri,headers,json);
            bc.response = Utils.PATCHRequest(uri,headers,json);
            return Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), Response.class);
        } catch(Exception e){
            logger.error("Error removePromo. "+ e.getMessage());
            throw e;
        }
    }
    private String Json_removePromo() {
        String json =  "{\n" +
                "\t\"cartId\": \"123\",\n" +
                "\t\"promoId\": \"1234\"\n" +
                "}";
        return json;
    }

    /***   mergeGuestCartWithUser   ***/
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class MergeGuestCartWithUser {
        private Object guestCartId;
    }
    public Response API_mergeGuestCartWithUser(String userAuthToken, Object bodyClass) throws Exception {
        try{
            logger.info("mergeGuestCartWithUser API request.");
            String uri = bc.domain + cartEndPoint+ "/merge";
            HashMap<String, String> headers = new HashMap<>();
            headers.put("x-site-context", Utils.xSiteContextCommerce(
                    bc.testEnvironment.MONGO_ACCOUNT_ID(),bc.testEnvironment.STAGE()));
            headers.put("Authorization",userAuthToken);
            String json= Utils.javaToJson(bodyClass);
            bc.payloadForTestReport(uri,headers,json);
            bc.response = Utils.PATCHRequest(uri,headers,json);
            return Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), Response.class);
        } catch(Exception e){
            logger.error("Error mergeGuestCartWithUser. "+ e.getMessage());
            throw e;
        }
    }
    private String Json_mergeGuestCartWithUser() {
        String json =  "{\n" +
                "\t\"guestCartId\": \"123\"\n" +
                "}";
        return json;
    }
    public MergeGuestCartWithUser Body_mergeGuestCartWithUser() throws IOException {
        return Utils.unmarshallGeneric(Json_mergeGuestCartWithUser(), MergeGuestCartWithUser.class);
    }

    /***   deleteCartById   ***/
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class DeleteCartById {
        private Object orderNumber;
    }
    public Response API_deleteCartById(Object cartId, Object bodyClass) throws Exception {
        try{
            logger.info("deleteCartById API request.");
            String uri = bc.domain+cartEndPoint+"/"+cartId.toString()+"/orderNumber";
            HashMap<String, String> headers = new HashMap<>();
            headers.put("x-site-context", Utils.xSiteContextCommerce(
                    bc.testEnvironment.MONGO_ACCOUNT_ID(),bc.testEnvironment.STAGE()));
            String json= Utils.javaToJson(bodyClass);
            bc.payloadForTestReport(uri,headers,json);
            bc.response = Utils.PATCHRequest(uri,headers,json);
            return Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), Response.class);
        } catch(Exception e){
            logger.error("Error deleteCartById. "+ e.getMessage());
            throw e;
        }
    }
    private String Json_deleteCartById() {
        String json =  "{\n" +
                "\t\"orderNumber\": \"1234-1234-12345\"\n" +
                "}";
        return json;
    }
    public DeleteCartById Body_deleteCartById() throws IOException {
        return Utils.unmarshallGeneric(Json_deleteCartById(), DeleteCartById.class);
    }

    /***   addShipTo2LineItemId   ***/
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class AddShipToLine {
        private Object itemId;
        private Object lineItemId;
        private Object shipToId;
    }
    public Response API_addShipToLineOld(Object cartId, Object bodyClass) throws Exception {
        try{
            logger.info("addShipToLineOld API request.");
            String uri = bc.domain + cartEndPoint+ "/" + cartId.toString() + "/ship-to";
            HashMap<String, String> headers = new HashMap<>();
            headers.put("x-site-context", Utils.xSiteContextCommerce(
                    bc.testEnvironment.MONGO_ACCOUNT_ID(),bc.testEnvironment.STAGE()));
            String json= Utils.javaToJson(bodyClass);
            bc.payloadForTestReport(uri,headers,json);
            bc.response = Utils.PATCHRequest(uri,headers,json);
            return Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), Response.class);
        } catch(Exception e){
            logger.error("Error addShipToLineOld. "+ e.getMessage());
            throw e;
        }
    }
    public Response API_addShipToLineV1(Object cartId, Object bodyClass) throws Exception {
        try{
            logger.info("addShipToLineV1 API request.");
            String uri = bc.domain + cartEndPoint+ "/" + cartId.toString() + "/ship-to/v1";
            HashMap<String, String> headers = new HashMap<>();
            headers.put("x-site-context", Utils.xSiteContextCommerce(
                    bc.testEnvironment.MONGO_ACCOUNT_ID(),bc.testEnvironment.STAGE()));
            String json= Utils.javaToJson(bodyClass);
            bc.payloadForTestReport(uri,headers,json);
            bc.response = Utils.PATCHRequest(uri,headers,json);
            return Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), Response.class);
        } catch(Exception e){
            logger.error("Error addShipToLineV1. "+ e.getMessage());
            throw e;
        }
    }
    public String Json_addShipToLine(int NbrOfItems) {
        String shipToObjects="";
        for (int j=0; j<NbrOfItems; j++){
            shipToObjects = shipToObjects + "\t{\n" +
                    "\t\t\"itemId\": 123,\n" +
                    "\t\t\"lineItemId\": 1,\n" +
                    "\t\t\"shipToId\": \"1234\"\n";
            if (j == NbrOfItems-1){ shipToObjects = shipToObjects + "\t}\n";
            } else { shipToObjects = shipToObjects + "\t},\n"; }
        }
        String json =  "[\n" +
                shipToObjects +
                "]";
        return json;
    }
    public AddShipToLine[] Body_addShipToLine(int NbrOfItems) throws IOException {
        return Utils.unmarshallGeneric(Json_addShipToLine(NbrOfItems), AddShipToLine[].class);
    }

    public Boolean isPromoApplied(Object[] promos, String expected) {
        for (Object promo : promos){
            if (promo.toString().equals(expected))
                return true;
        }
        return false;
    }
    public Response cartCreate_success(int nbrOfItems, Boolean isUserCart, String authToken) throws Exception {
        CreateCart cartBody= Body_createCart(nbrOfItems);
        for (int i = 0; i< cartBody.getItems().length; i++)
            cartBody.getItems()[i].setItemId(bc.items.get(i));
        Response cartResponse= API_createCart(cartBody,isUserCart,authToken);

        if (bc.response.getStatusLine().getStatusCode()!=200)
            throw new Exception("create Cart failure.");
        return cartResponse;
    }
    public Response addShipToLine_success(Response cartDetails, List<Object> shipToIds) throws Exception {
        AddShipToLine[] addShipBody = Body_addShipToLine(cartDetails.getItems().length);
        for (int i=0; i<addShipBody.length;i++) {
            addShipBody[i].setShipToId(shipToIds.get(i));
            addShipBody[i].setLineItemId(cartDetails.getItems()[i].getLineItemId());
            addShipBody[i].setItemId(cartDetails.getItems()[i].getItemId());
        }
        Response addShipResponse = API_addShipToLineOld(cartDetails.get_id(),addShipBody);

        if (bc.response.getStatusLine().getStatusCode()!=200)
            throw new Exception("addShipToLine failure.");

        return addShipResponse;
    }
}
