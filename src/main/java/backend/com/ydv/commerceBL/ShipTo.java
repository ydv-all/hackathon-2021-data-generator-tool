package backend.com.ydv.commerceBL;

import backend.com.ydv.utils.BaseClass;
import backend.com.ydv.utils.Utils;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;

public class ShipTo {
    final static Logger logger = Logger.getLogger(ShipTo.class);

    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Response {
        // Response body
        private ShipMethod shipMethod;
        private Address address;
        private Object shipToType;
        private Object taxCode;
        private Object cartId;
        private Object shipToId;
        private Object createdAt;
        private Object updatedAt;
        private Object _id;
        private Object isPickup;
        private Object storeId;
        private Object __v;

        // v1 added properties
        private PickupPerson pickupPerson;
        private PickupPerson altPickupPerson;
        private Object warehouseId;
    }

    // shipTo.pickupPerson
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class PickupPerson {
        private Name name;
        private Object email;
        private Phone phone;
    }

    // shipTo.address
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Address {
        private Phone phone;
        private Name name;
        private Object street1;
        private Object city;
        private Object state;
        private Object country;
        private Object email;
        private Object zipCode;
        private Object kind;
    }

    // shipTo.shipMethod
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ShipMethod
    {
        private Cost cost;
        private Object shipMethodId;
        private Object shipmentCarrier;
        private Object shipmentMethod;
    }

    // shipTo.address.phone
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Phone {
        private Object number;
        private Object kind;
    }

    // shipTo.address.name
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Name {
        private Object first;
        private Object last;
    }

    // shipTo.shipMethod.cost
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Cost {
        private Object currency;
        private Object amount;
        private Object realPrice;
        private Object discount;
    }

    //Util variables
    private final BaseClass bc;


    /* CONSTRUCTOR */
    public ShipTo(){
        bc = BaseClass.getInstance();
    }

    /***   createShipToOld   ***/
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class CreateShipToOld {
        private Address address;
        private Object shipToType;
        private ShipMethod shipMethod;
        private Object taxCode;
    }
    public Response API_createShipToOld(Object cartId, Object bodyClass) throws Exception {
        try {
            logger.info("createShipToOld API request.");
            String uri = bc.domain + "/api-cart/ship-to/cart/" + cartId.toString();
            HashMap<String, String> headers = new HashMap<>();
            headers.put("x-site-context", Utils.xSiteContextCommerce(
                    bc.testEnvironment.MONGO_ACCOUNT_ID(), bc.testEnvironment.STAGE()));
            String json= Utils.javaToJson(bodyClass);
            bc.payloadForTestReport(uri, headers, json);
            bc.response = Utils.POSTRequest(uri,headers,json);
            return Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), Response.class);
        } catch(Exception e) {
            logger.error("Error createShipToOld. " + e.getMessage());
            throw e;
        }
    }
    public String Json_ShipToOld() {
        return  "{\n" +
                "\t\"address\": {\n" +
                "\t\t\"street1\": \"10400 NE 4th St\",\n" +
                "\t\t\"city\": \"Bellevue\",\n" +
                "\t\t\"state\": \"WA\",\n" +
                "\t\t\"country\": \"United States\",\n" +
                "\t\t\"zipCode\": \"98004\",\n" +
                "\t\t\"kind\": \"shipping\",\n" +
                "\t\t\"name\": {\n" +
                "\t\t\t\"first\": \"John\",\n" +
                "\t\t\t\"last\": \"Smith\"\n" +
                "\t\t},\n" +
                "\t\t\"email\": \"johnsmith@fabric.inc\",\n" +
                "\t\t\"phone\": {\n" +
                "\t\t\t\"number\": \"123-992-9404\",\n" +
                "\t\t\t\"kind\": \"Mobile\"\n" +
                "\t\t}\n" +
                "\t},\n" +
                "\t\"shipToType\": \"SHIP_TO_ADDRESS\",\n" +
                "\t\"shipMethod\": {\n" +
                "\t\t\"shipMethodId\": 100000,\n" +
                "\t\t\"shipmentCarrier\": \"Fedex\",\n" +
                "\t\t\"shipmentMethod\": \"Next Day\",\n" +
                "\t\t\"cost\": {\n" +
                "\t\t\t\"currency\": \"USD\",\n" +
                "\t\t\t\"amount\": 5\n" +
                "\t\t}\n" +
                "\t},\n" +
                "\t\"taxCode\": \"FR1000\"\n" +
                "}";
    }
    public CreateShipToOld Body_createShipToOld() throws IOException {
        return Utils.unmarshallGeneric(Json_ShipToOld(), CreateShipToOld.class);
    }

    /***   Get ShipTo by Id   ***/
    public Response API_getShipToById(Object shipToId) throws Exception {
        try {
            logger.info("getShipToById API request.");
            String uri = bc.domain + "/api-cart/ship-to/" + shipToId.toString();
            HashMap<String, String> headers = new HashMap<>();
            headers.put("x-site-context", Utils.xSiteContextCommerce(
                    bc.testEnvironment.MONGO_ACCOUNT_ID(), bc.testEnvironment.STAGE()));
            bc.payloadForTestReport(uri, headers, "");
            bc.response = Utils.GETRequest(uri,headers);
            return Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), Response.class);
        } catch(Exception e) {
            logger.error("Error getShipToById. " + e.getMessage());
            throw e;
        }
    }

    /***   Get Cart By ShipTo  ***/
    public Response API_getCartByShipTo(Object shipToId) throws Exception {
        try {
            logger.info("getCartByShipTo API request.");
            String uri = bc.domain + "/api-cart/ship-to/"+shipToId.toString()+"/cart";
            HashMap<String, String> headers = new HashMap<>();
            headers.put("x-site-context", Utils.xSiteContextCommerce(
                    bc.testEnvironment.MONGO_ACCOUNT_ID(), bc.testEnvironment.STAGE()));
            bc.payloadForTestReport(uri, headers, "");
            bc.response = Utils.GETRequest(uri,headers);
            return Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), Response.class);
        } catch(Exception e) {
            logger.error("Error getCartByShipTo. " + e.getMessage());
            throw e;
        }
    }

    /***   createShipToV1Shipping   ***/
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class CreateShipToV1Shipping {
        // Response body
        private Address address;
        private Object shipToType;
        private Object shipMethod;
        private Object taxCode;
        private Object isPickup;
    }
    public Response API_createShipToV1(Object cartId, Object bodyClass) throws Exception {
        try {
            logger.info("createShipToV1Shipping API request.");
            String uri = bc.domain + "/api-cart/ship-to/v1/cart/" + cartId.toString();
            HashMap<String, String> headers = new HashMap<>();
            headers.put("x-site-context", Utils.xSiteContextCommerce(
                    bc.testEnvironment.MONGO_ACCOUNT_ID(), bc.testEnvironment.STAGE()));
            String json= Utils.javaToJson(bodyClass);
            bc.payloadForTestReport(uri, headers, json);
            bc.response = Utils.POSTRequest(uri,headers,json);
            return Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), Response.class);
        } catch(Exception e) {
            logger.error("Error createShipToV1Shipping. " + e.getMessage());
            throw e;
        }
    }
    public String Json_ShipTov1Shipping() {
        return  "{\n" +
                "\t\"shipToType\": \"SHIP_TO_ADDRESS\",\n" +
                "\t\"address\": {\n" +
                "\t\t\"street1\": \"888 Broadway\",\n" +
                "\t\t\"city\": \"New York\",\n" +
                "\t\t\"state\": \"NY\",\n" +
                "\t\t\"country\": \"United States\",\n" +
                "\t\t\"zipCode\": \"10003\",\n" +
                "\t\t\"kind\": \"shipping\",\n" +
                "\t\t\"name\": {\n" +
                "\t\t\t\"first\": \"First\",\n" +
                "\t\t\t\"last\": \"Last\"\n" +
                "\t\t},\n" +
                "\t\t\"email\": \"first@fabric.inc\",\n" +
                "\t\t\"phone\": {\n" +
                "\t\t\t\"number\": \"123-992-9404\",\n" +
                "\t\t\t\"kind\": \"Mobile\"\n" +
                "\t\t}\n" +
                "\t},\n" +
                "\t\"taxCode\": \"FR1000\",\n" +
                "\t\"shipMethod\":10002,\n" +
                "\t\"isPickup\":false\n" +
                "}" ;
    }
    public CreateShipToV1Shipping Body_createShipToV1Shipping() throws IOException {
        return Utils.unmarshallGeneric(Json_ShipTov1Shipping(), CreateShipToV1Shipping.class);
    }

    /***   createShipToV1PickUp  ***/
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class CreateShipToV1Pickup {
        // Response body
        private Object shipToType;
        private PickupPerson pickupPerson;
        private PickupPerson altPickupPerson;
        private Object taxCode;
        private Object warehouseId;
        private Object isPickup;
    }
    public String Json_ShipToPickup() {
        String json =  "{\n" +
                "\t\"shipToType\": \"BOPIS\"\n" +
                "\t\"pickupPerson\": {\n" +
                "\t\t\"name\": {\n" +
                "\t\t\t\"first\": \"John\",\n" +
                "\t\t\t\"last\": \"Doe\"\n" +
                "\t\t},\n" +
                "\t\t\"email\": \"johndoe@fabric.inc\",\n" +
                "\t\t\"phone\": {\n" +
                "\t\t\t\"number\": \"8087769338\",\n" +
                "\t\t\t\"kind\": \"Mobile\"\n" +
                "\t\t}\n" +
                "\t},\n" +
                "\t\"altPickupPerson\": {\n" +
                "\t\t\"name\": {\n" +
                "\t\t\t\"first\": \"first\",\n" +
                "\t\t\t\"last\": \"last\"\n" +
                "\t\t},\n" +
                "\t\t\"email\": \"first@fabric.inc\",\n" +
                "\t\t\"phone\": {\n" +
                "\t\t\t\"number\": \"8087769338\",\n" +
                "\t\t\t\"kind\": \"Mobile\"\n" +
                "\t\t}\n" +
                "\t},\n" +
                "\t\"taxCode\": \"FR1000\"\n" +
                "\t\"warehouseId\": \"60a6e23ca02bd20008849641\"\n" +
                "\t\"isPickup\": \"true\"\n" +
                "}";
        return json;
    }
    public CreateShipToV1Pickup Body_createShipToPickup() throws IOException {
        return Utils.unmarshallGeneric(Json_ShipToPickup(), CreateShipToV1Pickup.class);
    }

    public Response createShipTo_success(Object cartId) throws Exception {
        ShipTo.CreateShipToOld shipBody= Body_createShipToOld();

        ShipTo.Response shipDetails =
                API_createShipToOld(cartId,shipBody);

        if (bc.response.getStatusLine().getStatusCode()!=200)
            throw new Exception("createShipTo failure.");
        return shipDetails;
    }
}
