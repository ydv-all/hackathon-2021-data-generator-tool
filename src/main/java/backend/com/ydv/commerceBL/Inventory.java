package backend.com.ydv.commerceBL;

import backend.com.ydv.utils.BaseClass;
import backend.com.ydv.utils.Utils;
import lombok.Getter;
import lombok.Setter;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

import java.io.IOException;

@Setter @Getter
public class Inventory {
    final static Logger logger = Logger.getLogger(Inventory.class);

    private String _id;
    private Number itemId;
    private String createdAt;

    private inLocations[] inlocations;
    private String updatedAt;
    private String sku;
    private String title;

    // inventory.inlocations
    @Setter @Getter
    public static class inLocations {
        private String _id;
        private location location;
        private Number inStock;
        private channel[] channel;
        private inTransit[] inTransit;
        private String id;
    }

    // inventory.inlocations.location
    @Setter @Getter
    public static class location
    {
        private String _id;
        private Number[] channel;
        private Boolean isActive;
        private String name;
        private address address;
        private String kind;
        private Number locationId;
        private Boolean pickup;
        private String type;
        private String createdAt;
        private String updatedAt;
        private Number __v;
    }

    // inventory.inLocations.channel
    @Setter @Getter
    public static class channel
    {
        private Number quantityReserved;
        private String _id;
        private Number channelId;
        private allocation allocation;
        private String id;
    }

    // inventory.inLocations.inTransit
    @Setter @Getter
    public static class inTransit
    {
        private String _id;
        private String inTransitRef;
        private Number orderQuantity;
        private Number quantityReserved;
        private String orderDate;
        private String stockDate;
        private String id;
    }

    // inventory.inLocations.location.address
    @Setter @Getter
    public static class address
    {
        private String street1;
        private String street2;
        private String zipCode;
        private String city;
        private String state;
        private String country;
        private phone[] phone;
    }

    // inventory.inLocations.location.address.phone
    @Setter @Getter
    public static class phone
    {
        private String number;
        private String kind;
    }

    // inventory.inLocations.channel.allocation
    @Setter @Getter
    public static class allocation
    {
        private String mou;
        private Number unit;
    }

    //Util variables
    private BaseClass bc;
    private String inventoryEndPoint1="/api-inventory/inventory/generic/";
    private String inventoryEndPoint2="?query=itemId";

    /* CONSTRUCTOR */
    public Inventory(){
        bc = BaseClass.getInstance();
        bc.client = HttpClientBuilder.create().build();
    }

    public void getInventoryById(String itemID) throws IOException {
        try {
            HttpGet request = new HttpGet(bc.testEnvironment.BASE_ENDPOINT_COMMERCE_BL()+inventoryEndPoint1+itemID+inventoryEndPoint2);
            request.setHeader("x-site-context", Utils.xSiteContextCommerce(bc.testEnvironment.MONGO_ACCOUNT_ID(),bc.testEnvironment.STAGE()));
            bc.response = bc.client.execute(request);
        } catch(Exception e){
            logger.error("Error getting inventory. "+ e.getMessage());
            throw e;
        }

    }
}
