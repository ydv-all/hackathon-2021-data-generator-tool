package backend.com.ydv.commerceBL;

import backend.com.ydv.utils.BaseClass;
import backend.com.ydv.utils.Utils;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class Orders {
    final static Logger logger = Logger.getLogger(Orders.class);

    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Response {
        // Response body
        private Object checkoutComplete;
        private Object orderId;

        private Object cartId;
        private Object customerEmail;
        private CustomerPhoneNumber customerPhoneNumber;
        private Payment.PaymentDetails paymentDetails;
        private EstimatedTax estimatedTax;
        private ShipFrom shipFrom;
    }

    // Orders.customerPhoneNumber
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class CustomerPhoneNumber{
        private Object number;
        private Object kind;
    }

    // Orders.estimatedTax
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class EstimatedTax{
        private ItemsTaxes[] itemsTaxes;
        private ShipToTaxes[] shipToTaxes;
    }

    // Orders.shipFrom
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ShipFrom{
        private Object street1;
        private Object street2;
        private Object city;
        private Object state;
        private Object country;
        private Object zipCode;
    }

    // Orders.estimatedTax.itemsTaxes
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ItemsTaxes{
        private Object lineItemId;
        private Object amount;
    }

    // Orders.estimatedTax.shipToTaxes
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ShipToTaxes{
        private Object shipToId;
        private Object amount;
    }

    //Util variables
    private final BaseClass bc;
    private final Payment pay;
    private final Cart cartOps;
    private final ShipTo shipToOps;

    /* CONSTRUCTOR */
    public Orders(){
        bc = BaseClass.getInstance();
        pay = new Payment();
        cartOps = new Cart();
        shipToOps = new ShipTo();
    }

    public Cart.Response cartCreate(int nbrOfItems, Boolean isUserCart, String authToken) throws Exception {
        Cart.CreateCart cartBody= cartOps.Body_createCart(nbrOfItems);
        for (int i = 0; i< cartBody.getItems().length; i++)
            cartBody.getItems()[i].setItemId(bc.items.get(i));
        Cart.Response cartResponse= cartOps.API_createCart(cartBody,isUserCart,authToken);

        if (bc.response.getStatusLine().getStatusCode()!=200)
            throw new Exception("create Cart failure.");
        return cartResponse;
    }
    public ShipTo.Response shipToOld(String cartId) throws Exception {
        ShipTo.CreateShipToOld shipToBody = shipToOps.Body_createShipToOld();
        ShipTo.Response shipToResponse = shipToOps.API_createShipToOld(cartId, shipToBody);

        if (bc.response.getStatusLine().getStatusCode()!=200)
            throw new Exception("create shipTo failure.");
        return shipToResponse;
    }
    public ShipTo.Response shipToV1Shipping(String cartId) throws Exception {
        ShipTo.CreateShipToV1Shipping shipToBody = shipToOps.Body_createShipToV1Shipping();
        ShipTo.Response shipToResponse = shipToOps.API_createShipToV1(cartId, shipToBody);

        if (bc.response.getStatusLine().getStatusCode()!=200)
            throw new Exception("shipToV1 failure.");
        return shipToResponse;
    }
    public Cart.Response addShipToLineOld(List<String> shipToIds, Cart.Response cartDetails) throws Exception {
        Cart.AddShipToLine[] addShipToBody = cartOps.Body_addShipToLine(shipToIds.size());
        for (int i = 0; i< cartDetails.getItems().length; i++){
            addShipToBody[i].setItemId(cartDetails.getItems()[i].getItemId());
            addShipToBody[i].setLineItemId(cartDetails.getItems()[i].getLineItemId());
            addShipToBody[i].setShipToId(shipToIds.get(i));
        }
        cartDetails = cartOps.API_addShipToLineOld(cartDetails.get_id().toString(),addShipToBody);

        if (bc.response.getStatusLine().getStatusCode()!=200)
            throw new Exception("addShipToLineOld failure.");
        return cartDetails;
    }
    public Cart.Response addShipToLineV1(List<String> shipToIds, Cart.Response cart) throws Exception {
        Cart.AddShipToLine[] addShipToBody = cartOps.Body_addShipToLine(shipToIds.size());
        for (int i = 0; i< cart.getItems().length; i++){
            addShipToBody[i].setItemId(cart.getItems()[i].getItemId());
            addShipToBody[i].setLineItemId(cart.getItems()[i].getLineItemId());
            addShipToBody[i].setShipToId(shipToIds.get(i));
        }
        cart = cartOps.API_addShipToLineV1(cart.get_id().toString(),addShipToBody);

        if (bc.response.getStatusLine().getStatusCode()!=200)
            throw new Exception("addShipToLineV1 failure.");
        return cart;
    }
    public String stripePayment(Double amount) throws Exception {
        //  create stripe method
        String paymentMethod = pay.API_createStripeMethod().getId().toString();

        // create Intent
        Payment.StripeIntent intentBody = pay.Body_createStripeIntent();
        intentBody.setAmount(amount.toString().replace(".",""));
        intentBody.setPayment_method(paymentMethod);
        Payment.Response intentDetails = pay.API_createStripeIntent(intentBody);

        //Confirm Intent
        Payment.StripeConfirm confirm = pay.Body_confirmStripeIntent();
        confirm.setPayment_method(paymentMethod);
        pay.API_confirmStripeIntent(intentDetails.getId().toString(),confirm);

        if (bc.response.getStatusLine().getStatusCode()!=200)
            throw new Exception("stripePayment failed.");
        return intentDetails.getId().toString();
    }
    public void addPaymentToCart(String cartId, String intentId) throws Exception {
        Payment.AddPaymentToCart addPayBody = pay.Body_addPaymentToCart();
        addPayBody.setCartId(cartId);
        addPayBody.getPaymentDetails().getTransactionDetails().setPaymentToken(intentId);
        pay.API_addPaymentToCart(addPayBody);
        if(bc.response.getStatusLine().getStatusCode()!=200)
            throw new Exception("addPaymentToCart failed. ");
    }

    public double calculateTotal(Cart.Response cart, checkOuts order) {
        double total=0.0;
        // Total cart amount
        total +=  (Double.parseDouble(cart.getTotalAmount().getAmount().toString()));

        // ShipTo total  of the items
        for (Cart.Items item : cart.getItems())
            total += Double.parseDouble(item.getShipTo().getShipMethod().getCost().getAmount().toString());

        // ShipToTaxes
        for (ShipToTaxes shipTax : order.estimatedTax.shipToTaxes)
            total += Double.parseDouble(shipTax.getAmount().toString());

        // Loop into item taxes
        for (ItemsTaxes itemTax :  order.estimatedTax.itemsTaxes){
            for (Cart.Items item : cart.getItems()){// for each item will calculate total(taxAmount*itemQuantity).
                if (item.getLineItemId().toString().equals(
                        itemTax.lineItemId.toString())){
                    total += (Double.parseDouble(itemTax.getAmount().toString())
                                    * Double.parseDouble(item.getQuantity().toString()));
                    break;
                }
            }
        }
        return total;
    }

    /***   SHARED Json  ***/
    private String itemsTaxesJson(int nbrOfItems) {
        String json="";
        for(int i=0;i<nbrOfItems; i++){
            json =  json + "\t\t\t{\n" +
                    "\t\t\t\t\"lineItemId\": 1,\n" +
                    "\t\t\t\t\"amount\": 0\n";
            if (i == nbrOfItems-1){ json = json + "\t\t\t}\n";
            } else { json = json + "\t\t\t},\n"; }
        }
        return json;
    }
    private String shipToTaxesJson(int nbrOfShipTo) {
        String json="";
        for(int i=0;i<nbrOfShipTo; i++){
            json =  json + "\t\t\t{\n" +
                    "\t\t\t\t\"shipToId\": \"123\",\n" +
                    "\t\t\t\t\"amount\": 0\n";
            if (i == nbrOfShipTo-1){ json = json + "\t\t\t}\n";
            } else { json = json + "\t\t\t},\n"; }
        }
        return json;
    }

    /***   order Checkouts  ***/
    @Getter @Setter @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class checkOuts {
        private Object cartId;
        private Object customerEmail;
        private CustomerPhoneNumber customerPhoneNumber;
        private Payment.Response[] paymentDetails;
        private EstimatedTax estimatedTax;
    }
    public Response API_orderCheckout_v2(Object clazz) throws Exception {
        try {
            logger.info("orderCheckout_v2 API request.");
            String uri = bc.testEnvironment.BASE_ENDPOINT_COMMERCE_BL() + "/api-order/checkout";
            HashMap<String, String> headers = new HashMap<>();
            headers.put("x-site-context", Utils.xSiteContextCommerce(
                    bc.testEnvironment.MONGO_ACCOUNT_ID(), bc.testEnvironment.STAGE()));
            String json= Utils.javaToJson(clazz);
            bc.payloadForTestReport(uri, headers, json);
            bc.response = Utils.POSTRequest(uri,headers,json);
            return Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), Response.class);
        }catch(Exception e){
            logger.error("Error orderCheckout_v2. "+ e.getMessage());
            throw e;
        }
    }
    public Response API_orderCheckout_v4(Object clazz) throws Exception {
        try {
            logger.info("orderCheckout_v2 API request.");
            String uri = bc.testEnvironment.BASE_ENDPOINT_COMMERCE_BL() + "/api-order/checkout-v4";
            HashMap<String, String> headers = new HashMap<>();
            headers.put("x-site-context", Utils.xSiteContextCommerce(
                    bc.testEnvironment.MONGO_ACCOUNT_ID(), bc.testEnvironment.STAGE()));
            String json= Utils.javaToJson(clazz);
            bc.payloadForTestReport(uri, headers, json);
            bc.response = Utils.POSTRequest(uri,headers,json);
            return Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), Response.class);
        }catch(Exception e){
            logger.error("Error orderCheckout_v2. "+ e.getMessage());
            throw e;
        }
    }
    public String Json_OrderCheckouts(int payments, int itemTaxes, int shipToTaxes) {
        return "{\n" +
                "\t\"cartId\": \"123\",\n" +
                "\t\"customerEmail\": \"tawseef@fabric.inc\",\n" +
                "\t\"customerPhoneNumber\": {\n" +
                "\t\t\"number\": \"07780811973\",\n" +
                "\t\t\"kind\": \"Mobile\"\n" +
                "\t},\n" +
                "\t\"paymentDetails\": [\n" + pay.authorizeDotNetJson(payments) +
                "\t],\n" +
                "\t\"estimatedTax\": {\n" +
                "\t\t\"itemsTaxes\": [\n" + itemsTaxesJson(itemTaxes) +
                "\t\t],\n" +
                "\t\t\"shipToTaxes\": [\n" + shipToTaxesJson(shipToTaxes) +
                "\t\t]\n" +
                "\t}\n" +
                "}";
    }
    public checkOuts Body_orderCheckouts(int payments, int itemTaxes, int shipToTaxes) throws IOException {
        return Utils.unmarshallGeneric(Json_OrderCheckouts(payments,itemTaxes,shipToTaxes), checkOuts.class);
    }
}
