package backend.com.ydv.login;

import backend.com.ydv.utils.BaseClass;
import backend.com.ydv.utils.Utils;
import lombok.Getter;
import lombok.Setter;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

import java.util.HashMap;

@Setter @Getter
public class LoginMlt {
    final static Logger logger = Logger.getLogger(LoginMlt.class);

    //private Account account;
    private int userId;
    private String[] roles;
    private String[] permissions;
    private int accountId;
    //private Name name;
    private String accessToken;
    private String refreshToken;

    private String _id;


    private BaseClass bc;
    private String identityEndPoint="/api-identity/auth/local/login";

    /* CONSTRUCTOR */
    public LoginMlt(){
        bc = BaseClass.getInstance();
        bc.client = HttpClientBuilder.create().build();
    }

    /* API Request */
    public void loginCopilot(String user, String pwd, String accID) throws Exception {
        try {
            logger.info("Login copilot");
            String uri = bc.testEnvironment.BASE_ENDPOINT_COPILOT_BL() + identityEndPoint;
            HashMap<String, String> headers = new HashMap<>();
            headers.put("x-site-context", Utils.xSiteContext());

            HttpPost request = new HttpPost(uri);
            for (String header : headers.keySet()) {
                request.setHeader(header, headers.get(header));}
            String json = "{\"username\" :\""+user+"\", \"password\": \""+pwd+"\",\"accountId\": \""+accID+"\"}";
            bc.payloadForTestReport(uri,headers,json);
            request.setEntity(new StringEntity(json, ContentType.APPLICATION_JSON));
            bc.response = bc.client.execute(request);
        } catch (Exception e){
            logger.error("Failed at login copilot. " + e.getMessage());
            throw e;
        }
    }

    public LoginMlt attemptToLoginCopilot() throws Exception {
        try {
            LoginMlt lm;
            int attempts=1;
            do {
                loginCopilot(bc.testEnvironment.EMAIL_ID(), bc.testEnvironment.PASSWORD(),bc.testEnvironment.ACCOUNT_ID());
                System.out.println(bc.response.getStatusLine().getStatusCode());
                attempts++;
            } while(attempts <= 3 &&
                    bc.response.getStatusLine().getStatusCode() != 200);

            if(bc.response.getStatusLine().getStatusCode() != 200) throw new Exception("Unable to login to copilot after 3 attempts.");

            lm = Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), LoginMlt.class);
            return lm;
        } catch(Exception e){
            logger.error("Failed at login copilot. " + e.getMessage());
            throw e;
        }
    }

}
