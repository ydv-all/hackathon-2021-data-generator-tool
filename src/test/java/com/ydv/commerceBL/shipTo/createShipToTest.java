package com.ydv.commerceBL.shipTo;

import com.ydv.BaseTest;
import backend.com.ydv.commerceBL.Cart;
import backend.com.ydv.commerceBL.Login;
import backend.com.ydv.commerceBL.ShipTo;
import backend.com.ydv.utils.Listener;
import backend.com.ydv.utils.Utils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(Listener.class)
public class createShipToTest extends BaseTest {

    private Login lCom = new Login();//Login operations
    private Cart cartOps = new Cart();//Cart operations
    private ShipTo shipToOps = new ShipTo();//Cart operations

    // Pre - conditions
    @BeforeClass(alwaysRun = true)
    public void beforeClass() throws Exception {
        lCom = lCom.attemptToLoginCommerce();
    }

    @Test(groups = {"Regression", "Sanity"})
    public void createShipTo_success() throws Exception {
        Cart.Response cartDetails =
                cartOps.cartCreate_success(1,false,"");

        ShipTo.Response shipDetails =
                shipToOps.createShipTo_success(cartDetails.get_id());

        Utils.validate("Status code.",bc.response.getStatusLine().getStatusCode(),200);
        Utils.validate("Cart ID.",shipDetails.getCartId(),cartDetails.get_id());
    }
}
