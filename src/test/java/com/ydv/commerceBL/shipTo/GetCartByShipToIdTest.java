package com.ydv.commerceBL.shipTo;

import com.ydv.BaseTest;
import backend.com.ydv.commerceBL.Cart;
import backend.com.ydv.commerceBL.Login;
import backend.com.ydv.commerceBL.ShipTo;
import backend.com.ydv.utils.Listener;
import backend.com.ydv.utils.Utils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

@Listeners(Listener.class)
public class GetCartByShipToIdTest extends BaseTest {

    private Login lCom = new Login();//Login operations
    private Cart cartOps = new Cart();//Cart operations
    private ShipTo shipToOps = new ShipTo();//Cart operations

    // Pre - conditions
    @BeforeClass(alwaysRun = true)
    public void beforeClass() throws Exception {
        lCom = lCom.attemptToLoginCommerce();
    }

    @Test(groups = {"Regression", "Sanity"})
    public void getCartByShipTo_success() throws Exception {
        Cart.Response cartDetails =
                cartOps.cartCreate_success(1,false,"");

        List<Object> shipToIds = new ArrayList<>();
        ShipTo.Response shipDetails = shipToOps.createShipTo_success(cartDetails.get_id());
        shipToIds.add(shipDetails.get_id());

        cartOps.addShipToLine_success(cartDetails,shipToIds);

        ShipTo.Response getCartDetails =
                shipToOps.API_getCartByShipTo(shipDetails.get_id());

        Utils.validate("Status code.",bc.response.getStatusLine().getStatusCode(),200);
        Utils.validate("Cart ID.",getCartDetails.get_id(),cartDetails.get_id());
    }
}
