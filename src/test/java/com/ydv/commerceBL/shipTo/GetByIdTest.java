package com.ydv.commerceBL.shipTo;

import com.ydv.BaseTest;
import backend.com.ydv.commerceBL.Cart;
import backend.com.ydv.commerceBL.Login;
import backend.com.ydv.commerceBL.ShipTo;
import backend.com.ydv.utils.Listener;
import backend.com.ydv.utils.Utils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(Listener.class)
public class GetByIdTest extends BaseTest {

    private Login lCom = new Login();//Login operations
    private final Cart cartOps = new Cart();//Cart operations
    private final ShipTo shipToOps = new ShipTo();//Cart operations

    // Pre - conditions
    @BeforeClass(alwaysRun = true)
    public void beforeClass() throws Exception {
        lCom = lCom.attemptToLoginCommerce();
    }

    @Test(groups = {"Regression", "Sanity"})
    public void getShipTo_byId() throws Exception {
        Cart.Response cartDetails =
                cartOps.cartCreate_success(1,false,"");

        ShipTo.Response shipDetails =
                shipToOps.createShipTo_success(cartDetails.get_id());

        ShipTo.Response getShipDetails =
                shipToOps.API_getShipToById(shipDetails.get_id());

        Utils.validate("Status code.",bc.response.getStatusLine().getStatusCode(),200);
        Utils.validate("ShipTo ID.",getShipDetails.get_id(),shipDetails.get_id());
        Utils.validate("Cart ID.",getShipDetails.getCartId(),cartDetails.get_id());
    }
}
