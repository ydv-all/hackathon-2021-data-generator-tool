package com.ydv.commerceBL.cart;

import com.ydv.BaseTest;
import backend.com.ydv.commerceBL.Cart;
import backend.com.ydv.commerceBL.Login;
import backend.com.ydv.utils.Listener;
import backend.com.ydv.utils.Utils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

@Listeners(Listener.class)
public class UpdateItemsInCartTest extends BaseTest {

    private Login lCom = new Login();//Login operations
    private Cart cartOps = new Cart();//Cart operations

    // Pre - conditions
    @BeforeClass(alwaysRun = true)
    public void beforeClass() throws Exception {
        lCom = lCom.attemptToLoginCommerce();
    }

    @Test(groups = {"Regression", "Sanity"})
    public void updateSingleItem_GuestCart() throws Exception {
        int qty = Utils.randomInt(10);
        // create cart
        Cart.Response cartDetails =
                cartOps.cartCreate_success(2,false,"");

        // Create body and update params
        Cart.UpdateItemsInCart updateBody = cartOps.Body_updateItemsInCart(1);
        updateBody.getItems()[0].setLineItemId(
                cartDetails.getItems()[0].getLineItemId());
        updateBody.getItems()[0].setItemId(
                cartDetails.getItems()[0].getItemId());
        updateBody.getItems()[0].setQuantity(qty);

        // send request
        Cart.Response updateDetails =
                cartOps.API_updateItemsInCart(
                        cartDetails.get_id(),updateBody);

        Utils.validate("Status code.",
                bc.response.getStatusLine().getStatusCode(),200);
        Utils.validate("Cart ID.",
                cartDetails.get_id(),updateDetails.get_id());
        Utils.validate("Quantity.",
                updateDetails.getItems()[0].getQuantity(),qty);
    }

    @Test(groups = {"Regression", "Sanity"})
    public void updateSingleItem_UserCart() throws Exception {
        int qty = Utils.randomInt(5);
        // create cart
        Cart.Response cartDetails =
                cartOps.cartCreate_success(2,true, lCom.getAccessToken());

        // Create body and update params
        Cart.UpdateItemsInCart updateBody = cartOps.Body_updateItemsInCart(1);
        updateBody.getItems()[0].setLineItemId(
                cartDetails.getItems()[0].getLineItemId());
        updateBody.getItems()[0].setItemId(
                cartDetails.getItems()[0].getItemId());
        updateBody.getItems()[0].setQuantity(qty);

        // send request
        Cart.Response updateDetails =
                cartOps.API_updateItemsInCart(
                        cartDetails.get_id(),updateBody);

        Utils.validate("Status code.",
                bc.response.getStatusLine().getStatusCode(),200);
        Utils.validate("Cart ID.",
                cartDetails.get_id(),updateDetails.get_id());
        Utils.validate("Quantity.",
                updateDetails.getItems()[0].getQuantity(),qty);
    }
}
