package com.ydv.commerceBL.cart;

import com.ydv.BaseTest;
import backend.com.ydv.commerceBL.Cart;
import backend.com.ydv.commerceBL.Login;
import backend.com.ydv.utils.Listener;
import backend.com.ydv.utils.Utils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

@Listeners(Listener.class)
public class MergeGuestCartWithUserTest extends BaseTest {

    private Login lCom = new Login();//Login operations
    private Cart cartOps = new Cart();//Cart operations

    // Pre - conditions
    @BeforeClass(alwaysRun = true)
    public void beforeClass() throws Exception {
        lCom = lCom.attemptToLoginCommerce();
    }

    @Test(groups = {"Regression", "Sanity"})
    public void mergeGuestCart_success() throws Exception {
        Cart.Response cartDetails =
                cartOps.cartCreate_success(1,false,"");

        // create and update Body
        Cart.MergeGuestCartWithUser mergeBody = cartOps.Body_mergeGuestCartWithUser();
        mergeBody.setGuestCartId(cartDetails.get_id());

        // Send request
        Cart.Response mergeDetails =
                cartOps.API_mergeGuestCartWithUser(lCom.getAccessToken(), mergeBody);

        Utils.validate("Status code.",
                bc.response.getStatusLine().getStatusCode(),200);
        Utils.validate("registeredUser.",
                mergeDetails.getRegisteredUser(),true);
        assertNotNull(mergeDetails.getUserId());
    }
}
