package com.ydv.commerceBL.cart;

import com.ydv.BaseTest;
import backend.com.ydv.commerceBL.Login;
import backend.com.ydv.commerceBL.Cart;
import backend.com.ydv.utils.Listener;
import backend.com.ydv.utils.Utils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

@Listeners(Listener.class)
public class CreateCartTest extends BaseTest {
    //Login operations
    private Login lCom = new Login();

    //Cart operations
    private final Cart cartOps = new Cart();

    // Pre - conditions
    @BeforeClass(alwaysRun = true)
    public void beforeClass() throws Exception {
        lCom = lCom.attemptToLoginCommerce();
    }

    @Test(groups = {"Regression", "Sanity"})
    public void createGuestCart_singleItem_noPriceListId() throws Exception {
        Cart.CreateCart cartBody = cartOps.Body_createCart(1);
        cartBody.getItems()[0].setItemId(bc.testEnvironment.ITEM_ID1());
        Cart.Response cartDetails =
                cartOps.API_createCart(cartBody,false,"");

        Utils.validate("Status code.",
                bc.response.getStatusLine().getStatusCode(),200);
        Utils.validate("priceListId.",
                cartDetails.getItems()[0].getPriceListId(),bc.testEnvironment.DEFAULT_PRICE_LIST_ID());
    }

    @Test(groups = {"Regression", "Sanity"})
    public void createUserCart_singleItem_noPriceListId() throws Exception {
        Cart.CreateCart cartBody = cartOps.Body_createCart(1);
        //DBHandle.getItemsForCart(1).get(0)
        cartBody.getItems()[0].setItemId(bc.testEnvironment.ITEM_ID1());
        Cart.Response cartDetails =
                cartOps.API_createCart(cartBody,true, lCom.getAccessToken());

        Utils.validate("Status code.",
                bc.response.getStatusLine().getStatusCode(),200);
        Utils.validate("priceListId.",
                cartDetails.getItems()[0].getPriceListId(),bc.testEnvironment.DEFAULT_PRICE_LIST_ID());
        assertNotNull(cartDetails.getUserId());
    }

    @Test(groups = {"Regression", "Sanity"})
    public void createGuestCart_multipleItems_noPriceListId() throws Exception {
        Cart.CreateCart cartBody = cartOps.Body_createCart(3);
        //DBHandle.getItemsForCart(1).get(0)
        cartBody.getItems()[0].setItemId(bc.testEnvironment.ITEM_ID1());
        cartBody.getItems()[1].setItemId(bc.testEnvironment.ITEM_ID2());
        cartBody.getItems()[2].setItemId(bc.testEnvironment.ITEM_ID3());
        Cart.Response cartDetails =
                cartOps.API_createCart(cartBody,false,"");

        Utils.validate("Status code.",
                bc.response.getStatusLine().getStatusCode(),200);
        for (Cart.Items item : cartDetails.getItems())
            Utils.validate("priceListId.",
                    item.getPriceListId(),bc.testEnvironment.DEFAULT_PRICE_LIST_ID());
    }

    @Test(groups = {"Regression", "Sanity"})
    public void createUserCart_multipleItems_noPriceListId() throws Exception {
        Cart.CreateCart cartBody = cartOps.Body_createCart(2);
        //DBHandle.getItemsForCart(1).get(0)
        cartBody.getItems()[0].setItemId(bc.testEnvironment.ITEM_ID1());
        cartBody.getItems()[1].setItemId(bc.testEnvironment.ITEM_ID2());

        Cart.Response cartDetails =
                cartOps.API_createCart(cartBody,true, lCom.getAccessToken());

        Utils.validate("Status code.",
                bc.response.getStatusLine().getStatusCode(),200);
        for (Cart.Items item : cartDetails.getItems())
            Utils.validate("priceListId.",
                    item.getPriceListId(),bc.testEnvironment.DEFAULT_PRICE_LIST_ID());
        assertNotNull(cartDetails.getUserId());
    }

    @Test(groups = {"Regression"})
    public void createUserCart_invalidAuthToken() throws Exception {
        Cart.CreateCart cartBody = cartOps.Body_createCart(1);
        //DBHandle.getItemsForCart(1).get(0)
        cartBody.getItems()[0].setItemId(bc.testEnvironment.ITEM_ID1());
        Cart.Response cartDetails =
                cartOps.API_createCart(cartBody,true,"123");

        Utils.validate("Status code.",
                bc.response.getStatusLine().getStatusCode(),401);
        Utils.validate("Code:",
                cartDetails.getCode(),"INVALID_AUTHORIZATION_TOKEN");
        Utils.validate("Message:",
                cartDetails.getMessage(),"Invalid Authorization Token");
    }
}
