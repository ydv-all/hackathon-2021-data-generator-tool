package com.ydv.commerceBL.cart;

import com.ydv.BaseTest;
import backend.com.ydv.commerceBL.Cart;
import backend.com.ydv.commerceBL.Login;
import backend.com.ydv.utils.Listener;
import backend.com.ydv.utils.Utils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

@Listeners(Listener.class)
public class GetCartTest extends BaseTest {

    private Login lCom = new Login();//Login operations
    private Cart cartOps = new Cart();//Cart operations

    // Pre - conditions
    @BeforeClass(alwaysRun = true)
    public void beforeClass() throws Exception {
        lCom = lCom.attemptToLoginCommerce();
    }

    @Test(groups = {"Regression", "Sanity"})
    public void getCartByID_Success() throws Exception {
        Cart.Response cartDetails =
                cartOps.cartCreate_success(1,false,"");

        Cart.Response getCartDetails =
                cartOps.API_getCartBy("cartId",cartDetails.get_id());

        Utils.validate("Status code.",
                bc.response.getStatusLine().getStatusCode(),200);
        Utils.validate("Cart ID.",
                cartDetails.get_id(),getCartDetails.get_id());
    }

    @Test(groups = {"Regression", "Sanity"})
    public void getCartByAuthToken_Success() throws Exception {
        Cart.Response cartDetails =
                cartOps.cartCreate_success(1,true,lCom.getAccessToken());

        Cart.Response getCartDetails =
                cartOps.API_getCartBy("userAuthToken",lCom.getAccessToken());

        Utils.validate("Status code.",
                bc.response.getStatusLine().getStatusCode(),200);
        Utils.validate("Cart ID.",
                cartDetails.get_id(),getCartDetails.get_id());
        Utils.validate("User.",
                cartDetails.getUserId(),getCartDetails.getUserId());
    }
}