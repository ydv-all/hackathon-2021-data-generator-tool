package com.ydv.commerceBL.cart;

import com.ydv.BaseTest;
import backend.com.ydv.commerceBL.Cart;
import backend.com.ydv.commerceBL.Login;
import backend.com.ydv.utils.Listener;
import backend.com.ydv.utils.Utils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

@Listeners(Listener.class)
public class DeleteCartByIdTest extends BaseTest {

    private Login lCom = new Login();//Login operations
    private Cart cartOps = new Cart();//Cart operations

    // Pre - conditions
    @BeforeClass(alwaysRun = true)
    public void beforeClass() throws Exception {
        lCom = lCom.attemptToLoginCommerce();
    }

    @Test(groups = {"Regression", "Sanity"})
    public void Delete_GuestCart() throws Exception {
        Cart.Response cartDetails =
                cartOps.cartCreate_success(1,false,"");

        Cart.DeleteCartById deleteBody = cartOps.Body_deleteCartById();

        Cart.Response deletedDetails =
                cartOps.API_deleteCartById(
                        cartDetails.get_id(),deleteBody);

        Utils.validate("Status code.",
                bc.response.getStatusLine().getStatusCode(),200);
        Utils.validate("Cart ID.",
                cartDetails.get_id(),deletedDetails.get_id());
        Utils.validate("Deleted.",
                deletedDetails.getDeleted(),true);
    }

    @Test(groups = {"Regression", "Sanity"})
    public void Delete_UserCart() throws Exception {
        Cart.Response cartDetails =
                cartOps.cartCreate_success(1,true, lCom.getAccessToken());

        Cart.DeleteCartById deleteBody = cartOps.Body_deleteCartById();

        Cart.Response deletedDetails =
                cartOps.API_deleteCartById(
                        cartDetails.get_id(),deleteBody);

        Utils.validate("Status code.",
                bc.response.getStatusLine().getStatusCode(),200);
        Utils.validate("Cart ID.",
                cartDetails.get_id(),deletedDetails.get_id());
        Utils.validate("Deleted.",
                deletedDetails.getDeleted(),true);
    }
}
