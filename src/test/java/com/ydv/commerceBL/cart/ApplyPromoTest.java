package com.ydv.commerceBL.cart;

import com.ydv.BaseTest;
import backend.com.ydv.commerceBL.Cart;
import backend.com.ydv.commerceBL.Login;
import backend.com.ydv.utils.Listener;
import backend.com.ydv.utils.Utils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

@Listeners(Listener.class)
public class ApplyPromoTest extends BaseTest {

    private Login lCom = new Login();//Login operations
    private Cart cartOps = new Cart();//Cart operations

    // Pre - conditions
    @BeforeClass(alwaysRun = true)
    public void beforeClass() throws Exception {
        lCom = lCom.attemptToLoginCommerce();
    }

    @Test(groups = {"Regression", "Sanity"})
    public void applyPromo_GuestCart() throws Exception {
        Cart.Response cartDetails =
                cartOps.cartCreate_success(2,false,"");

        Cart.ApplyPromo promoBody = cartOps.Body_applyPromo();
        promoBody.setCartId(cartDetails.get_id());
        promoBody.setPromoName(bc.testEnvironment.COUPON());

        Cart.Response promoDetails =
                cartOps.API_applyPromo(promoBody);

        Utils.validate("Status code.",
                bc.response.getStatusLine().getStatusCode(),200);
        Utils.validate("Cart ID.",
                cartDetails.get_id(),promoDetails.get_id());
        throw new Exception("Automated test not completed. Missing validation");
    }
}
