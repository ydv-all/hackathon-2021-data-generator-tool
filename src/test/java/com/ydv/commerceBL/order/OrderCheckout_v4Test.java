package com.ydv.commerceBL.order;

import com.ydv.BaseTest;
import backend.com.ydv.commerceBL.Cart;
import backend.com.ydv.commerceBL.Login;
import backend.com.ydv.commerceBL.Orders;
import backend.com.ydv.utils.Listener;
import backend.com.ydv.utils.Utils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

@Listeners(Listener.class)
public class OrderCheckout_v4Test extends BaseTest {

    //Login operations
    private Login lc = new Login();

    //Cart,ShipTo,Orders operations
    private final Orders orderOps = new Orders();

    @BeforeClass(alwaysRun = true)
    public void beforeClass() throws Exception {
        //login to commerce to populate userAuthToken
        lc = lc.attemptToLoginCommerce();
    }

    @Test(groups = {"Regression", "Sanity"})
    public void checkoutV4_singleItem_shipToOld_AddShipToOld_AuthNet() throws Exception {
        // Checkout 2, GuestCart, singleItem, shipToOld, AddShipToLineItemOld, AuthorizedNet
        // cart creation.
        Cart.Response cartDetails = orderOps.cartCreate(1,false,"");
        // shipTo creation
        List<String> shipToIds = new ArrayList<>();
        shipToIds.add(orderOps.shipToOld(cartDetails.get_id().toString()).get_id().toString());
        // add shipTo to line item
        cartDetails = orderOps.addShipToLineOld(shipToIds,cartDetails);

        //Create Order
        Orders.checkOuts orderBody = orderOps.Body_orderCheckouts(1,1,1);
        orderBody.setCartId(cartDetails.get_id());
        orderBody.getEstimatedTax().getShipToTaxes()[0].setShipToId(
                cartDetails.getItems()[0].getShipTo().get_id());
        orderBody.getPaymentDetails()[0].setAmount(orderOps.calculateTotal(cartDetails,orderBody));
        Orders.Response orderResponse = orderOps.API_orderCheckout_v4(orderBody);

        Utils.validate("checkoutComplete. ",
                orderResponse.getCheckoutComplete(),true);
    }

    @Test(groups = {"Regression", "Sanity"})
    public void checkoutV4_singleItem_shipToV1Shipping_AddShipToV1_AuthNet() throws Exception {
        // Checkout 2, GuestCart, singleItem, shipToOld, AddShipToLineItemOld, AuthorizedNet
        // cart creation.
        Cart.Response cartDetails = orderOps.cartCreate(1,false,"");
        // shipTo creation
        List<String> shipToIds = new ArrayList<>();
        shipToIds.add(orderOps.shipToV1Shipping(cartDetails.get_id().toString()).get_id().toString());
        // add shipTo to line item
        cartDetails = orderOps.addShipToLineV1(shipToIds,cartDetails);

        //Create Order
        Orders.checkOuts orderBody = orderOps.Body_orderCheckouts(1,1,1);
        orderBody.setCartId(cartDetails.get_id());
        orderBody.getEstimatedTax().getShipToTaxes()[0].setShipToId(
                cartDetails.getItems()[0].getShipTo().get_id());
        orderBody.getPaymentDetails()[0].setAmount(orderOps.calculateTotal(cartDetails,orderBody));
        Orders.Response orderResponse = orderOps.API_orderCheckout_v4(orderBody);

        Utils.validate("checkoutComplete. ",
                orderResponse.getCheckoutComplete(),true);
    }

    @Test(groups = {"Regression", "Sanity"})
    public void checkoutV4_singleItem_shipToV1Shipping_AddShipToV1_Stripe() throws Exception {
        // Checkout 2, GuestCart, singleItem, shipToOld, AddShipToLineItemOld, AuthorizedNet
        // cart creation.
        Cart.Response cartDetails = orderOps.cartCreate(1,false,"");
        // shipTo creation
        List<String> shipToIds = new ArrayList<>();
        shipToIds.add(orderOps.shipToV1Shipping(cartDetails.get_id().toString()).get_id().toString());
        // add shipTo to line item
        cartDetails = orderOps.addShipToLineV1(shipToIds,cartDetails);

        //Create Order
        Orders.checkOuts orderBody = orderOps.Body_orderCheckouts(0,1,1);
        orderBody.setCartId(cartDetails.get_id());
        orderBody.getEstimatedTax().getShipToTaxes()[0].setShipToId(
                cartDetails.getItems()[0].getShipTo().get_id());

        //Stripe and addCart
        orderOps.addPaymentToCart(cartDetails.get_id().toString(),
                orderOps.stripePayment(
                        orderOps.calculateTotal(cartDetails,orderBody)));

        Orders.Response orderResponse = orderOps.API_orderCheckout_v4(orderBody);

        Utils.validate("checkoutComplete. ",
                orderResponse.getCheckoutComplete(),true);
    }


}
