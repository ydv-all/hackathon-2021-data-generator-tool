package com.ydv.copilotBL.configurations.settings;

import com.ydv.BaseTest;

import backend.com.ydv.copilotBL.configurations.settings.CancellationPolicy;
import backend.com.ydv.utils.Listener;
import backend.com.ydv.login.LoginMlt;
import backend.com.ydv.utils.Utils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.testng.Assert.assertEquals;

@Listeners(Listener.class)
public class cancellationPolicyTest extends BaseTest {

    /**  Variable declaration */
    //To login and get Token for request
    private LoginMlt lm = new LoginMlt();

    //Cancellation Policy operations
    private CancellationPolicy cp = new CancellationPolicy();

    //Parameters sent in request body
    private Boolean isEnabled, autoRef;
    private int windowHrs;
    /******/

    @BeforeClass(alwaysRun = true)
    public void beforeClass() throws Exception {
        //login to copilot to populate token
        lm = lm.attemptToLoginCopilot();
    }

    @Test(groups = {"Regression", "Sanity"})
    public void createCancellationPolicy() throws IOException
    {
        // creating Random values
        isEnabled=Utils.randomBool(); autoRef=Utils.randomBool();
        windowHrs=Utils.randomInt(100);

        //sending request
        cp.initCancelPolicyParams();
        cp.setIsEnabled(isEnabled);cp.setAutoRefund(autoRef);cp.setWindowHours(windowHrs);
        cp.createCancellationPolicy(lm.getAccessToken());
        cp = Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), CancellationPolicy.class);

        assertEquals(bc.response.getStatusLine().getStatusCode(), 200);
        assertEquals(cp.getIsEnabled(),isEnabled);
        assertEquals(cp.getAutoRefund(),autoRef);
        assertEquals(cp.getWindowHours(),windowHrs);
    }

    @Test(groups = {"Regression"})
    public void createCancellationPolicy_isEnabled_invalid() throws IOException
    {
        //sending request
        cp.initCancelPolicyParams();
        cp.setIsEnabled(123);
        cp.createCancellationPolicy(lm.getAccessToken());
        cp = Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), CancellationPolicy.class);

        assertEquals(bc.response.getStatusLine().getStatusCode(), 400);
        assertEquals(cp.getCode(), "REQUEST_VALIDATION");
        assertEquals(cp.getMessage(), "Invalid data in body: /isEnabled should be boolean");
    }

    @Test(groups = {"Regression"})
    public void createCancellationPolicy_windowHours_invalid() throws IOException
    {
        //sending request
        cp.initCancelPolicyParams();
        cp.setWindowHours("\"Hola\"");
        cp.createCancellationPolicy(lm.getAccessToken());
        cp = Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), CancellationPolicy.class);

        assertEquals(bc.response.getStatusLine().getStatusCode(), 400);
        assertEquals(cp.getCode(), "REQUEST_VALIDATION");
        assertEquals(cp.getMessage(), "Invalid data in body: /windowHours should be number");
    }

    @Test(groups = {"Regression", "Sanity"})
    public void updateCancellationPolicy() throws IOException
    {
        // creating Random values
        isEnabled=Utils.randomBool(); autoRef=Utils.randomBool();
        windowHrs=Utils.randomInt(100);

        //sending request
        cp.initCancelPolicyParams();
        cp.setIsEnabled(isEnabled);cp.setAutoRefund(autoRef);cp.setWindowHours(windowHrs);
        cp.updateCancellationPolicy(lm.getAccessToken());
        cp = Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), CancellationPolicy.class);

        assertEquals(bc.response.getStatusLine().getStatusCode(), 200);
        assertEquals(cp.getIsEnabled(),isEnabled);
        assertEquals(cp.getAutoRefund(),autoRef);
        assertEquals(cp.getWindowHours(),windowHrs);
    }

    @Test(groups = {"Regression"})
    public void updateCancellationPolicy_isEnabled_invalid() throws IOException
    {
        //sending request
        cp.initCancelPolicyParams();
        cp.setIsEnabled(123);
        cp.updateCancellationPolicy(lm.getAccessToken());
        cp = Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), CancellationPolicy.class);

        assertEquals(bc.response.getStatusLine().getStatusCode(), 400);
        assertEquals(cp.getCode(), "REQUEST_VALIDATION");
        assertEquals(cp.getMessage(), "Invalid data in body: /isEnabled should be boolean");
    }

    @Test(groups = {"Regression"})
    public void updateCancellationPolicy_windowHours_invalid() throws IOException
    {
        //sending request
        cp.initCancelPolicyParams();
        cp.setWindowHours("\"Hola\"");
        cp.updateCancellationPolicy(lm.getAccessToken());
        cp = Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), CancellationPolicy.class);

        assertEquals(bc.response.getStatusLine().getStatusCode(), 400);
        assertEquals(cp.getCode(), "REQUEST_VALIDATION");
        assertEquals(cp.getMessage(), "Invalid data in body: /windowHours should be number");
    }

    @Test(groups = {"Regression", "Sanity"})
    public void getCancellationPolicy() throws IOException
    {
        // creating Random values
        isEnabled=Utils.randomBool(); autoRef=Utils.randomBool();
        windowHrs=Utils.randomInt(100);

        //sending createCancellationPolicy request
        cp.initCancelPolicyParams();
        cp.setIsEnabled(isEnabled);cp.setAutoRefund(autoRef);cp.setWindowHours(windowHrs);
        cp.createCancellationPolicy(lm.getAccessToken());

        //GET a cancellation policy
        cp.getCancellationPolicy(lm.getAccessToken());
        cp = Utils.unmarshallGeneric(bc.obtainJsonFromResponse(), CancellationPolicy.class);

        assertEquals(bc.response.getStatusLine().getStatusCode(), 200);
        assertEquals(cp.getIsEnabled(),isEnabled);
        assertEquals(cp.getAutoRefund(),autoRef);
        assertEquals(cp.getWindowHours(),windowHrs);
    }

    @Test(enabled = false, groups = {"Regression", "Sanity"})
    public void testCase_Example() throws Exception
    {
        /*Cart c = new Cart();
        c.activeItemsForCart(lm.getAccessToken(),2);
        c.initializeBodyParams(3,1,0);
        c.getItems()[0].setItemId("Anything i want!");
        System.out.println(c.generateJson_Cart());*/
    }
}
