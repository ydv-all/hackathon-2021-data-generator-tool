package com.ydv;

import backend.com.ydv.utils.BaseClass;
import backend.com.ydv.utils.Listener;
import org.testng.annotations.*;

import java.io.IOException;

@Listeners(Listener.class)
public class BaseTest {

    public BaseClass bc = BaseClass.getInstance();

    @BeforeSuite(alwaysRun = true)
    //@Parameters({"environment"})
    public void beforeSuite() {
        //bc.getProperties(environment);
        bc.getProperties("qaPim2Env");//qaPim2Env, sandBox
        //DBHandle.getInstance();
        bc.items.add(bc.testEnvironment.ITEM_ID1());
        bc.items.add(bc.testEnvironment.ITEM_ID2());
        bc.items.add(bc.testEnvironment.ITEM_ID3());
        bc.domain = bc.testEnvironment.BASE_ENDPOINT_COMMERCE_BL();
    }

    @BeforeMethod
    public void BeforeMethod() throws IOException {
        bc.responseStringForReport = "";
        bc.payloadStringForReport = "";
    }

    @AfterClass
    public void afterClass() throws IOException {
        bc.closeResources();
    }
}
