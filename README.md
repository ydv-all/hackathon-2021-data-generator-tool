# The Coders - Data Generator Tool #

Data generator tool in order to have use cases data/test data for validation of tickets during sprints.

### What is this repository for? ###

* Backend folder contains logic to create our test data
* Frontend folder contains a friendly interface to help with configs

### How do I get set up? ###

* MacOS setup

- Install homebrew
```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```
- Install Java 8
```
brew install openjdk@8
```
- Create a symlink to
```
sudo ln -sfn /usr/local/opt/openjdk@8/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk-8.jdk
```
- Install Maven
```
brew install maven
```
- Install Spring
```
brew tap spring-io/tap
brew install spring-boot
```

## Run the backend
```
mvn spring-boot:run
```

### Who do I talk to? ###

* Denis Augusto
* Mario Garay
* Fernando Mendoza
* Joao Victor Zanatta